
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('MODULO_DEFECTO','home');
define('LAYOUT_DEFECTO','plantilla.php');
define('MODULO_PATH',realpath('./Modulo/'));
define('LAYOUT_PATH',realpath('./Layout/'));
$conf['home'] = array(
				'archivo' => 'Home/Controller.php',
				'layout' => LAYOUT_DEFECTO);

    $conf['perfil'] = array('archivo'=>'perfil/controller.php');


/*Zona para la seccion de Titulos electronicos*/
    $conf['Registro'] = array('archivo'=>'TitulosElectronicos/Registro/Controller.php');
    $conf['Ecertificado'] = array('archivo'=>'TitulosElectronicos/Ecertificado/Controller.php');
/*Zona para la seccion de Certificacion electronica*/    
    $conf['Certificado'] = array('archivo'=>'Academico/Certificado/Controller.php');
    
/*Zona para la seccion de Academico*/
    $conf['Alumnos'] = array('archivo'=>'Academico/Alumnos/Controller.php');
    $conf['Carreras'] = array('archivo'=>'Academico/Carreras/Controller.php');
/*Zona para la seccion de Catalogos*/
    $conf['Entidades'] = array('archivo'=>'Catalogos/EntidadesFederativas/Controller.php');
    $conf['Firmantes'] = array('archivo'=>'Catalogos/FirmantesAutorizados/Controller.php');
    $conf['Antecedentes'] = array('archivo'=>'Catalogos/AntecedentesEstudio/Controller.php');
    $conf['Nivel'] = array('archivo'=>'Catalogos/NivelDeEstudios/Controller.php');
    $conf['Fundamento'] = array('archivo'=>'Catalogos/FundamentoLegal/Controller.php');
    $conf['Modalidad'] = array('archivo'=>'Catalogos/ModalidadDeTitulacion/Controller.php');
    $conf['Autorizacion'] = array('archivo'=>'Catalogos/AutorizacionDeReconocimiento/Controller.php');
    $conf['Cancelacion'] = array('archivo'=>'Catalogos/MotivosDeCancelacion/Controller.php');
    $conf['Genero'] = array('archivo'=>'Catalogos/Genero/Controller.php');
    $conf["Periodo"] = array('archivo'=>'Catalogos/Periodo/Controller.php');
    $conf["Certificacion"] = array('archivo'=>'Catalogos/Certificacion/Controller.php');
    $conf["Observaciones"] = array('archivo'=>'Catalogos/Observaciones/Controller.php');
    $conf["Asignatura"] = array('archivo'=>'Catalogos/Asignatura/Controller.php');
    $conf["Documentos"] = array('archivo'=>'Catalogos/Documentos/Controller.php');
/*Zona para la seccion de Configuraciones*/
    $conf["Instituciones"] = array('archivo'=>'Configuracion/Instituciones/Controller.php');
    $conf["Sucursales"] = array('archivo'=>'Configuracion/Sucursales/Controller.php');
    $conf["Usuarios"] = array('archivo'=>'Configuracion/Usuarios/Controller.php');

    //$conf['inscribir'] = array('archivo'=>'inscripcion/index.php');
//$conf['preventa'] = array('archivo'=>'preventa.php');

