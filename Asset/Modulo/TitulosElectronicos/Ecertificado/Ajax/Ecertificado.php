<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ecertificado
 *
 * @author francisco
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Ecertificado {
    //put your code here
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    private $cadenaOriginal = "";
    private $cadenaOriginalDreoe = "";
    private $firma;
    private $sello;

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }   
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        
        $this->formulario = json_decode(file_get_contents('php://input'));
        //var_dump($_SESSION);
        switch ($this->formulario->Certificado->opc){
           
            case 'getData':
                $datatemp = $this->getAlumno();
                //var_dump($datatemp);
                if(count($datatemp)!=0){
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["Alumno"] = $datatemp;
                    $this->jsonData["Sucursal"] = $this->getSucursal($datatemp["_idCampus"]);
                    $this->jsonData["Carrera"] = $this->getCarrera($datatemp["_idCarrera"]);
                    $this->jsonData["Certificado"] = $this->getTipoCertificacion();
                    $this->jsonData["Calificaciones"] = $this->getCalificaciones($datatemp["_idCarrera"], $this->jsonData["Carrera"]["Modulos"], $datatemp["_id"]);
                }else{
                    $this->jsonData["Bandera"] = 0;
                    $this->josnData["Mensaje"] = "Alumno no encontrado";    
                }
                break;
            case 'setXML':
                $this->getCadenaOriginal();
                if($this->getFirma()){
                    $this->getSello();
                    $this->getCertificado();

                    $this->setXML();
                };
                
                break;
            
        }
         print json_encode($this->jsonData);        
    }
    
    
    private function getAlumno (){
        $sql = "SELECT A.*, CG.idGenero, CG.Genero  FROM Alumnos as A
                inner join Catalogo_Genero as CG on (CG._id = A._idGenero) 
                where A._id = ". $this->formulario->Alumno;
        return $this->conn->fetch($this->conn->query($sql));
    }
    
    private function getSucursal($idCampus){
       $sql = "select I._idInstitucion, I.Institucion, I.Nombre, I.Ap_Materno, I.Ap_Paterno, I.CURP, I._idCargo, CFA.Cargo_firmante, C.Nombre as Campus,
                C.Registro_DGP as idCampus, C.Domicilio, C.Colonia, C.CP, C.Ciudad, C._idEstado as idEntidadFederativa, EF.NOM_ENT as Estado
                     from Campus as C 
                inner join Instituciones as I on (I._id = C._idInstitucion)
                inner join Cargo_firmantes_autorizados as CFA on (CFA._id = I._idCargo)
                inner join EntidadesFederativas as EF on (EF.IDENTIDADFEDERATIVA = C._idEstado)
                where C._id = $idCampus";
        return $this->conn->fetch($this->conn->query($sql));
    }
    
    private function getCarrera ($idCarrera){
        $sql = "SELECT * FROM Carreras where _id = $idCarrera";
        return $this->conn->fetch($this->conn->query($sql));
    }
    
    private function getTipoCertificacion (){
        $array = array();
        $sql = "select * from Catalogo_Tipo_Certificacion";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array,$row);
        }
        //var_dump($array);
        return $array;
    }

    private function getCalificaciones($idCarrera, $modulos, $USR){
        //var_dump($idCarrera, $modulos, $USR);
        $suma = 0;
        $a = array();
        $array = array("grados"=>array(), "No_Asignaturas"=>0, "Asignadas"=>0, "Promedio"=>0, "TotalCreditos"=>0,"CreditosObtenidos"=>0);

        /*Obtenemos la cantidad de materias que tiene la carrera */
        $sql = "SELECT count(_id) as NoMaterias, sum(Creditos) as TotalCreditos from Asignaturas where _id_carrera = $idCarrera";
        $row = $this->conn->fetch($this->conn->query($sql));  
        $array["No_Asignaturas"] = $row["NoMaterias"];
        $array["TotalCreditos"] = $row["TotalCreditos"];

        /*Consultamos por modulo las calificaciones de cada materia */
        for($i = 0 ; $i<$modulos; $i++){
            $sql = "select M.id_Asignatura, M.modulo, M.Asignatura, M.Clave_asignatura , M._id_tipoAsignatura, TA.Descripcion,
                CAL.Calificacion, CAL.Ciclo, M.Creditos, CAL._idObservaciones
                from Asignaturas as M 
                inner join Calificaciones as CAL on (CAL._idAsignatura = M._id)
                inner join Catalogo_Tipo_Asignatura as TA on (M._id_tipoAsignatura = TA.idTipoAsignatura)
                where M._id_carrera = $idCarrera and CAL._idAlumno =$USR and M.modulo = ".($i+1)." order by M.id_Asignatura";
            $id = $this->conn->query($sql);
            if($this->conn->count_rows()!=0){
               $a = array();
                while($row  = $this->conn->fetch($id)){
                    $suma += $row["Calificacion"];
                    if($row["Calificacion"]>5){
                        /*Si la materia esta acreditada contamos cuantos creditos son de la materia e incremetamos cuntas materias ha cursado */
                        $array["CreditosObtenidos"] += $row["Creditos"]; //Sumamos los creditos obtenidos de la materia
                        $array["Asignadas"]++; //Inbrementamos cuantas materias ha acreditado
                    }
                    
                    array_push($a, array("materia"=>$row));
                } 
                $array["grados"][$i] = $a;
            }else{
                $array["grados"][$i] = array();
            }
        }
        
        $array["Promedio"] = $suma / $array["No_Asignaturas"];
        return $array;
    }
    
    private function getCadenaOriginal (){
        
        $this->cadenaOriginal  = "||2.0|5|".$this->formulario->sucursal->_idInstitucion."|".$this->formulario->sucursal->idCampus."|"
            .$this->formulario->sucursal->idEntidadFederativa."|".$this->formulario->sucursal->CURP."|".$this->formulario->sucursal->_idCargo. "|"
            .$this->formulario->Carrera->Rvoe."|". $this->formulario->Carrera->FechaExpedicionRVOE."T00:00:00|".$this->formulario->Carrera->_idCarrera."|"
            .$this->formulario->Carrera->_idTipoPeriodo."|".$this->formulario->Carrera->ClavePlan."|".$this->formulario->Carrera->_idNivelEstudios. "|"
            .$this->formulario->Carrera->CalificacionMinima."|".$this->formulario->Carrera->CalificacionMaxima."|".$this->formulario->Carrera->CalificacionMinimaAprobatoria."|"
            .$this->formulario->Alumno->USR."|".$this->formulario->Alumno->Curp."|".$this->formulario->Alumno->Nombre."|".$this->formulario->Alumno->Ap_Paterno."|"
            .$this->formulario->Alumno->Ap_Materno."|".$this->formulario->Alumno->idGenero."|".$this->formulario->Alumno->FechaNacimiento."T00:00:00|||"
            .$this->formulario->Expedicion->IdTipoCertificado."|".$this->formulario->Expedicion->FechaExpedicion."|".$this->formulario->sucursal->idEntidadFederativa."|"
            .$this->formulario->Calificaciones->No_Asignaturas."|".$this->formulario->Calificaciones->Asignadas."|".number_format($this->formulario->Calificaciones->Promedio,2,".",",")."|"
            .number_format($this->formulario->Calificaciones->TotalCreditos,2,'.',',')."|".number_format($this->formulario->Calificaciones->CreditosObtenidos,2,'.',',')."|";
            foreach($this->formulario->Calificaciones->grados as $key => $value){
                foreach ($value as $materia){
                    $this->cadenaOriginal .= $materia->materia->id_Asignatura."|".$materia->materia->Ciclo."|".number_format($materia->materia->Calificacion,2,'.',',')."|".
                    $materia->materia->_id_tipoAsignatura."|".number_format($materia->materia->Creditos)."|";
                }
            }
        $this->cadenaOriginal .= "|";
        var_dump($this->cadenaOriginal);
    }
    
    private function getCadenaOriginalDreoe(){
        $fecha = date("Y-m-d");
        $this->getCadenaOriginalDreoe = "||".$fecha."T00:00:00|".$this->Sello;
    }

    private function getFirma(){
        $private_file = "../../../../ClavesPrivadas/Claveprivada_FIEL_AUGJ620412K50_20190301_084850.key.pem";
        $private_key = openssl_get_privatekey(file_get_contents($private_file));
        $exito = openssl_sign($this->cadenaOriginal ,$this->firma,$private_key, OPENSSL_ALGO_SHA256);
        openssl_free_key($private_key);
        //var_dump($this->firma);
        return $exito;
    }

    private function getSello(){
        $this->sello = base64_encode($this->firma);
    }

    private function getCertificado(){
        $public_file ="../../../../ClavesPrivadas/augj620412k50.cer.pem";
        $public_key = openssl_pkey_get_public(file_get_contents($public_file));
        $PubData = openssl_pkey_get_details($public_key);
        $result = openssl_verify($this->cadenaOriginal, $this->firma, $public_key, "sha256WithRSAEncryption");

        //var_dump($public_key);   
    }

    private function setXML(){
        $fecha = date("Y-m-d");
        $fecha2 = date("Ymd");
        $objetoXML = new XMLWriter();

        // Estructura básica del XML
        $objetoXML->openURI("../../../../XML/Certificados/certificado_".$this->formulario->Alumno->USR.".xml");
        $objetoXML->setIndent(true);
        $objetoXML->setIndentString("\t");
        $objetoXML->startDocument('1.0', 'utf-8','yes');
        $objetoXML->startElement("Dec");
        $objetoXML->writeAttribute("version", "2.0");
        $objetoXML->writeAttribute("tipoCertificado", "5");
        $objetoXML->writeAttribute("folioControl", "1");
        $objetoXML->writeAttribute("sello", $this->sello);
        $objetoXML->writeAttribute("certificadoResponsable", "revisar manual");
        $objetoXML->writeAttribute("noCertificadoResponsable", "revisar manual");
        $objetoXML->writeAttribute("xmlns", "https://www.siged.sep.gob.mx/certificados");
            $objetoXML->startElement("Ipes");
            $objetoXML->writeAttribute("idNombreInstitucion", $this->formulario->sucursal->_idInstitucion);
            $objetoXML->writeAttribute("idCampus", $this->formulario->sucursal->idCampus);
            $objetoXML->writeAttribute("idEntidadFederativa", $this->formulario->sucursal->idEntidadFederativa);
                $objetoXML->startElement("Responsable");
                    $objetoXML->writeAttribute("curp", $this->formulario->sucursal->CURP); //ATRIBUTO DEL CURP DEL RESPONSABLE DE FIRMAR
                    $objetoXML->writeAttribute("nombre", $this->formulario->sucursal->Nombre); //ATRIBUTO DEL NOMBRE DEL RESPONSABLE DE FIRMAR
                    $objetoXML->writeAttribute("primerApellido", $this->formulario->sucursal->Ap_Paterno); //ATRIBUTO DEL APELLIDO PATERNO DEL RESPONSABLE DE FIRMAR
                    $objetoXML->writeAttribute("segundoApellido", $this->formulario->sucursal->Ap_Materno); //ATRIBUTO DEL APELLIDO MATERNO DEL RESPONSABLE DE FIRMAR
                    $objetoXML->writeAttribute("idCargo", $this->formulario->sucursal->_idCargo); //ATRIBUTO DEL CURP DEL RESPONSABLE DE FIRMAR
                $objetoXML->endElement();
            $objetoXML->endElement();
            $objetoXML->startElement("Rvoe");
                $objetoXML->writeAttribute("numero", $this->formulario->Carrera->Rvoe); //ATRIBUTO DEL NUMERO DEL REVOE
                $objetoXML->writeAttribute("fechaExpedicion", $this->formulario->Carrera->FechaExpedicionRVOE."T00:00:00"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
            $objetoXML->endElement();
            $objetoXML->startElement("Carrera");
                $objetoXML->writeAttribute("idCarrera", $this->formulario->Carrera->_idCarrera); //ATRIBUTO DEL NUMERO DEL REVOE
                $objetoXML->writeAttribute("idTipoPeriodo", $this->formulario->Carrera->_idTipoPeriodo); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("clavePlan", $this->formulario->Carrera->ClavePlan); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("idNivelEstudios", $this->formulario->Carrera->_idNivelEstudios); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("calificacionMinima", $this->formulario->Carrera->CalificacionMinima); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("calificacionMaxima", $this->formulario->Carrera->CalificacionMaxima); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("calificacionMinimaAprobatoria", $this->formulario->Carrera->CalificacionMinimaAprobatoria); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
            $objetoXML->endElement();
            $objetoXML->startElement("Alumno");
                $objetoXML->writeAttribute("numeroControl", $this->formulario->Alumno->USR); //ATRIBUTO DEL NUMERO DEL REVOE
                $objetoXML->writeAttribute("curp", $this->formulario->Alumno->Curp); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("nombre", $this->formulario->Alumno->Nombre); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("primerApellido", $this->formulario->Alumno->Ap_Paterno); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("segundoApellido", $this->formulario->Alumno->Ap_Materno); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("idGenero", $this->formulario->Alumno->idGenero); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("fechaNacimiento", $this->formulario->Alumno->FechaNacimiento."T00:00:00"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
            $objetoXML->endElement();
            $objetoXML->startElement("Expedicion");
                $objetoXML->writeAttribute("idTipoCertificacion", $this->formulario->Expedicion->IdTipoCertificado); //ATRIBUTO DEL NUMERO DEL REVOE
                $objetoXML->writeAttribute("fecha", $this->formulario->Expedicion->FechaExpedicion."T00:00:00"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("idLugarExpedicion", $this->formulario->sucursal->idEntidadFederativa); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
            $objetoXML->endElement();
            $objetoXML->startElement("Asignaturas");
                $objetoXML->writeAttribute("total", $this->formulario->Calificaciones->No_Asignaturas); //ATRIBUTO DEL NUMERO DEL REVOE
                $objetoXML->writeAttribute("asignadas", $this->formulario->Calificaciones->Asignadas); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("promedio", number_format($this->formulario->Calificaciones->Promedio,2,".",",")); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("totalCreditos", $this->formulario->Calificaciones->TotalCreditos); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                $objetoXML->writeAttribute("creditosObtenidos", $this->formulario->Calificaciones->CreditosObtenidos); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
                foreach($this->formulario->Calificaciones->grados as $key => $value){
                    foreach ($value as $materia){
                        $objetoXML->startElement("Asignatura");
                            $objetoXML->writeAttribute("idAsignatura", $materia->materia->id_Asignatura); //ATRIBUTO DEL NUMERO DEL REVOE
                            $objetoXML->writeAttribute("ciclo", $materia->materia->Ciclo); //ATRIBUTO DEL NUMERO DEL REVOE
                            $objetoXML->writeAttribute("calificacion", $materia->materia->Calificacion); //ATRIBUTO DEL NUMERO DEL REVOE
                            $objetoXML->writeAttribute("idObservaciones", $materia->materia->_idObservaciones); //ATRIBUTO DEL NUMERO DEL REVOE
                            $objetoXML->writeAttribute("idTipoAsignatura", $materia->materia->_id_tipoAsignatura); //ATRIBUTO DEL NUMERO DEL REVOE
                            $objetoXML->writeAttribute("creditos", $materia->materia->Creditos); //ATRIBUTO DEL NUMERO DEL REVOE
                        $objetoXML->endElement();
                    }
                }
            $objetoXML->endElement();
            $objetoXML->startElement("Dreoe");
                $objetoXML->writeAttribute("fechaDreoe", "");
                $objetoXML->writeAttribute("SelloDec", "");
                $objetoXML->writeAttribute("noCertificadoDreoe", "");
                $objetoXML->writeAttribute("curp", "");
                $objetoXML->writeAttribute("nombreCompleto", "");
                $objetoXML->writeAttribute("idCargo", "");
                $objetoXML->writeAttribute("cargo", "");
                $objetoXML->writeAttribute("selloDreoe", "");
            $objetoXML->endElement();

            $objetoXML->startElement("SepIpes");
                $objetoXML->writeAttribute("version", "");
                $objetoXML->writeAttribute("folioDigital", "");
                $objetoXML->writeAttribute("fechaSepIpes", "");
                $objetoXML->writeAttribute("selooDreoe", "");
                $objetoXML->writeAttribute("noCertificadoSepIpes", "");
                $objetoXML->writeAttribute("selloSepIpes", "");
           $objetoXML->endElement();


        $objetoXML->endElement(); // Final del nodo raíz, "Dec"
        $objetoXML->endDocument(); // Final del documento
    }
}

$app = new Ecertificado($array_principal);
$app->principal();