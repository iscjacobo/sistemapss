var url = "./Modulo/TitulosElectronicos/Ecertificado/Ajax/Ecertificado.php";

tituloElectronico
        .controller('EcertificadoCtrl',['$scope','$http',EcertificadoCtrl])
        .controller('EcertificadonewCtrl',['$scope','$http',EcertificadonewCtrl]);
function EcertificadoCtrl($scope,$http){
    var obj = $scope;
    
    obj.btnNewCertificado = ()=>{
        location.href="?mod=Ecertificado&opc=nuevo";
    };
    
    
    
    //Funcion que se inizaliza sola
    angular.element(document).ready(function(){
        obj.getSucursal();
        
        
     });
    
}

function EcertificadonewCtrl($scope,$http){
    
    var obj = $scope;
    obj.Sucursal = {};
    obj.Alumno = {};
    obj.Carrera = {};
    obj.Calificaciones = [];
    obj.TipoCertificado = [];
    obj.Expedicion = {
        IdTipoCertificado: "",
        //FechaExpedicion : moment().format("YYYY-MM-DDTHH:mm:ss")
        FechaExpedicion : moment().format("YYYY-MM-DD")
    };

    obj.idUSR = "";
    
    obj.getData =()=>{
         $http({
            method: 'POST',
                url: url,
                data: {Certificado: {opc:"getData"}, Alumno:obj.idUSR}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Sucursal = res.data.Sucursal;
                    obj.Alumno = res.data.Alumno;
                    obj.Carrera = res.data.Carrera;
                    obj.Calificaciones = res.data.Calificaciones
                    obj.TipoCertificado = res.data.Certificado;
                    console.log(obj.Calificaciones);
                    
                    //toastr.success(res.data.mensaje);
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    
    obj.getGrados = (mod)=>{
        return _.range(mod);
    }

    obj.setXML = ()=>{
        if(confirm("¿Estas seguro de generar el archivo XML?")){
            $http({
                method: 'POST',
                    url: url,
                    data: {Certificado: {opc:"setXML"}, sucursal: obj.Sucursal, 
                    Alumno: obj.Alumno, Carrera: obj.Carrera, 
                    Calificaciones: obj.Calificaciones, Expedicion: obj.Expedicion}
                }).then(function successCallback(res){
                    if(res.data.Bandera == 1){
                        obj.Sucursal = res.data.Sucursal;
                        obj.Alumno = res.data.Alumno;
                        obj.Carrera = res.data.Carrera;
                        obj.Calificaciones = res.data.Calificaciones
                        console.log(obj.Calificaciones);
                        
                        //toastr.success(res.data.mensaje);
                    }else{
                       toastr.error(res.data.mensaje);
                    }
                }, function errorCallback(res){
            });
        }
    }
    
    //Funcion que se inizaliza sola
    angular.element(document).ready(function(){
        console.log(obj.idUSR);
        obj.getData();
        
    });
}