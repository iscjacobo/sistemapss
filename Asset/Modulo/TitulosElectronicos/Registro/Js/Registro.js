/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var url = "./Modulo/TitulosElectronicos/Registro/Ajax/Registro.php";
tituloElectronico.controller('RegistroCtrl',['$scope','$http',RegistroCtrl]);
function RegistroCtrl($scope,$http){
    var obj = $scope;

    //Variables
    obj.rol="";

    obj.arrayCampus=[];
    obj.Campus = {};

    obj.arrayCarreras=[];
    obj.Carrera = {};

    obj.arrayAlumnos=[];
    obj.Alumno = {};

    obj.Antecedente = {};
    obj.Titulacion = {};
    obj.Carrera = {};
    obj.Servicio = {};
    obj.Documentos = {};
    obj.arrayTipoEstudio = {};
    obj.arrayEntidad= {};
    obj.arrayAutorizacion= {};
    obj.arrayNivel= {};    
    obj.arrayModalidad= {}; 
    obj.arrayFundamento= {}; 

    //Llamar al model de busquedas
    obj.btnModal = () =>{
        $("#ModalBuscar").modal("show");   
    }
    //Funcion busqueda de campus
    obj.getCampus = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getCampus"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.arrayCampus = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //funcion busqueda de Carreras
    obj.getCarreras = () =>{
        console.log(obj.Campus);
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getCarreras"}, Campus : obj.Campus}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayCarreras = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Funcion busqueda
    obj.getAlumnos = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getAlumnos"}, Carrera : obj.Carrera}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayAlumnos = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getAlumno = (e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getAlumno"}, _idAlumno :e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Alumno = res.data.Data;
                    obj.Antecedente = res.data.antecedentes;  
                    obj.getCarreraAlumno(obj.Alumno._idCarrera); 
                 

                    $("#ModalBuscar").modal("hide");   
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getCatalogos= ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getCatalogos"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.arrayEntidad = res.data.Entidad;
                    obj.arraySexo = res.data.Sexo;
                    obj.arrayTipoEstudio = res.data.Estudio;
                    obj.arrayAutorizacion = res.data.Autorizacion;
                    obj.arrayNivel=res.data.nivel;
                    obj.arrayModalidad = res.data.Modalidad;
                    obj.arrayFundamento = res.data.Fundamento;

                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getCarreraAlumno = (e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getCarrera"}, Carrera :e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Carrera = res.data.Carrera;
                 
                 

                    $("#ModalBuscar").modal("hide");   
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.regitrarDatosXML = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"Registro"}, _idAlumno : obj.Alumno._id, Carrera : {id: obj.Carrera._id, FechaInicioCarrera: obj.Carrera.FechaInicio,
                       FechaTerminoCarrera: obj.Carrera.FechaTerminacion}, _idAntecedentes : obj.Antecedente._id,
                       Titulacion : obj.Titulacion, Servicio : obj.Servicio}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){

                    toastr.success(res.data.mensaje);
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }

    obj.getDocumentos = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Registro: {opc:"getDocumentos"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Documentos = res.data.Data;
                    toastr.success(res.data.mensaje);
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Funcion que se inizaliza sola
    angular.element(document).ready(function(){
        obj.getDocumentos();
        obj.getCatalogos();
        switch(obj.rol){
            case 'root':
                    obj.getInstituciones();
                break;
            case 'Admin':
                    obj.getCampus();
                break;
           
        }
        
     });
}