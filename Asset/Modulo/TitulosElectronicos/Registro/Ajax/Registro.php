<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of Carreras
 *
 * @author Manuel Jacobo
 */


error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');


class Registro {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }   
    public function __destruct() {
        unset($this->conn);
    }
    public function principal(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch ($this->formulario->Registro->opc){
                case 'getInstituciones':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getInstituciones();
                            $this->jsonData["mensaje"]="Instituciones Listas";            
                break;
                case 'getCampus':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCampus();
                            $this->jsonData["mensaje"]="Campus Listos";            
                break;
                case 'getCarreras':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCarreras();
                            $this->jsonData["mensaje"]="Carreras Listas";
                break;
                case 'getAlumnos':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAlumnos();
                            $this->jsonData["mensaje"]="Alumnos listos";
                break;
                case 'getAlumno':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAlumno();
                            $this->jsonData["antecedentes"] = $this->getAntecedentes();
                            $this->jsonData["mensaje"]="Alumnos listos";
                break;
                case 'getCatalogos':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Entidad"] = $this->getEntidad();
                            $this->jsonData["Estudio"] = $this->getEstudio();
                            $this->jsonData["Autorizacion"] = $this->getAutorizacion();
                            $this->jsonData["nivel"] = $this->getnivel();
                            $this->jsonData["Modalidad"] = $this->getModalidad();
                            $this->jsonData["Fundamento"] = $this->getFundamento();
                            
                break;
                case 'getCarrera':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Carrera"] = $this->getCarrera();
                break;    
                case 'Registro':
                    if ($this->setRegistro()) {
                        $this->jsonData["Bandera"] = 1;
                        $this->jsonData["mensaje"] = "Se registro exitosamente";
                    } else {
                        $this->jsonData["Bandera"] = 0;
                        $this->jsonData["mensaje"] =  "Lo siento no se pudo registrar correctamente";
                    }
                break;
                case 'getDocumentos':
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["Data"] = $this->getDocumentos();
                break;
           
               
               
             
        }        
        print json_encode($this->jsonData);        
    }

    private function getInstituciones(){
       $array = array();
       $sql = "SELECT _id, Institucion FROM Instituciones where Estatus = '1'";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
         array_push($array, $row);
        }  
        return $array;
    }
    private function getCampus(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];//Ubicamos que rol tiene el usuario
        $_idInstitucion = $_SESSION["_idInstitucion"]; // ubicamos desde que institucion esta mnodificando

                switch ("$rol") {
                    case 'root':
                    $sql="SELECT _id, Nombre from Campus WHERE _idInstitucion = '{$this->formulario->Instituto->_idInstituto}' and Estatus = 1";
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                        break;

                    case 'Admin';
                        $sql="SELECT _id, Nombre FROM Campus WHERE _idInstitucion = ' $_idInstitucion' AND Estatus = 1 ";    
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                    break;

                                  
                    
            }
        return $array;
    }
    private function getCarreras(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];
        $_iCampus = $_SESSION["_idCampus"];
                switch ("$rol") {
                    case 'root':
                    case 'Admin';
                       $sql = "SELECT  _idCarrera, _id, Carrera, Modulos, _idCampus FROM Carreras WHERE Estatus = '1'
                       AND  _idCampus = '{$this->formulario->Campus->_idCampus}'";
                         $id = $this->conn->query($sql);
                         while($row = $this->conn->fetch($id)){
                          array_push($array, $row);
                         }  
                    break;

            
                    
            }
        return $array;
    }
    private function getAlumnos(){
        $array = array();
        $sql = "SELECT  * FROM Alumnos where _idCarrera = '{$this->formulario->Carrera->id}' and Estatus = 1";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getAlumno(){
        $array = array();
        $sql = "SELECT  * FROM Alumnos where _id = '{$this->formulario->_idAlumno}'  ";
        
        return $this->conn->fetch($this->conn->query($sql));
    }
    private function getAntecedentes(){
        $array = array();
         $sql = "SELECT  * FROM AntecedentesAlumno where _idAlumno = '{$this->formulario->_idAlumno}'  ";
        
        return $this->conn->fetch($this->conn->query($sql));
    }
    private function getEntidad(){
        $array = array();
            $sql = "SELECT * FROM EntidadesFederativas";
            $id = $this->conn->query($sql);
            while($row = $this->conn->fetch($id)){
                array_push($array, $row);
            }
        return $array;
    }
    private function getEstudio(){
        $array = array();
        $sql = "SELECT * FROM Estudio_antecedentes WHERE tipoDocumento = 1";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getnivel(){
        $array = array();
        $sql = "SELECT * FROM Estudio_antecedentes WHERE tipoDocumento = 2";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getCarrera(){
        $array = array();
         $sql = "SELECT  * FROM Carreras where _id = '{$this->formulario->Carrera}'  ";
        
        return $this->conn->fetch($this->conn->query($sql));
    }
    private function getAutorizacion(){
        $array = array();
        $sql = "SELECT * FROM Autorizacion_reconocimiento";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getModalidad(){
        $array = array();
        $sql = "SELECT * FROM Modalidad_de_titulacion";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getFundamento(){
        $array = array();
        $sql = "SELECT * FROM Fundamento_legal_servicio_social";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
   
    private function setRegistro(){
        $Fecha = date("Y-m-d");
        $User= $_SESSION["usr"];

          $sql = "INSERT INTO RegistroXMLtitulacion(
            FechaExpedicion, _idAlumno, _idAntecedentes, _idCarrera, FechaInicioCarrera, FechaTerminoCarrera, CumplioServicioSocial, _idFundamentoLegal, FechaExamen,
            FechaExtencionExamen, _idModalidadTitulacion, USRCreacion, FechaCreacion, USRModificacion, FechaModificacion)
            VALUES (
                '{$this->formulario ->Titulacion->FechaExpedicion}',
                '{$this->formulario ->_idAlumno}',
                '{$this->formulario ->_idAntecedentes}',
                '{$this->formulario ->Carrera->id}',
                '{$this->formulario ->Carrera->FechaInicioCarrera}',
                '{$this->formulario ->Carrera->FechaTerminoCarrera}',
                '{$this->formulario ->Servicio->cumplioServicioSocial}',
                '{$this->formulario ->Servicio->Fundamentolegal}',
                '{$this->formulario ->Titulacion->FechaExamen}',
                '{$this->formulario ->Titulacion->ExtencionFechaExamen}',
                '{$this->formulario ->Titulacion->ModalidadTitulacion}',
                '{$User}',
                '{$Fecha}',
                '{$User}',
                '{$Fecha}'
            )
            ";

          return $this->conn->query($sql);
    }
    
    private function getDocumentos(){
        $array = array();
        $sql = "SELECT * FROM Tipo_documento";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
  
        

        

    
};

$app = new Registro($array_principal);
$app->principal();

      