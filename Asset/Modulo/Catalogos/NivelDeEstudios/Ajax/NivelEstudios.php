<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Niveles {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->NivelEstudio->opc) {
            case 'new':
            case "edit":
                if ($this->setNiveles()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->NivelEstudio->opc == 'new'? 
                    "El nivel de estudio se registro satisfactoriamente":"El  nivel de estudio se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->NivelEstudio->opc == 'new'? 
                    "Error al registrar nivel de estudio":"Error al editar  nivel de estudio";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getNiveles();
                break;
            case 'doc':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getTipo_Documentos();
                break;
                
        }

        print json_encode($this->jsonData);
    }

    private function setNiveles() {
        $obj = json_decode($this->formulario);
        switch ($obj->NivelEstudio->opc) {
            case 'new':
                $sql = "INSERT INTO Catalogo_Nivel_De_Estudios(idNivelEstudios, Descripcion) "
                        . "values (UPPER('{$obj->NivelEstudio->idNivelEstudios}'), 
                                   UPPER('{$obj->NivelEstudio->Descripcion}'))";
                break;
            case 'edit';
                 $sql = "UPDATE Catalogo_Nivel_De_Estudios SET 
                 idNivelEstudios= UPPER('{$obj->NivelEstudio->idNivelEstudios}'),
                 Descripcion= UPPER('{$obj->NivelEstudio->Descripcion}')
                 where _id = {$obj->NivelEstudio->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getNiveles(){
        $array = array();
         $sql = "select * from Catalogo_Nivel_De_Estudios";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    


}

$app = new Niveles($array_principal);
$app->principal();
