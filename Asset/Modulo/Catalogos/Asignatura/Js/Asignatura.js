/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/Asignatura/Ajax/Asignatura.php"
tituloElectronico.controller('AsignaturaCtrl',['$scope','$http',AsignaturaCtrl]);

function AsignaturaCtrl($scope,$http){
    var obj = $scope;
    obj.Asignatura = {
        idTipoAsignatura : 0,
        Descripcion: "",
        opc: ""
    };
    obj.arrayAsignatura = [];
    
    obj.btnNuevaAsignatura = ()=>{
        $("#ModalAsignatura").modal("show");
        obj.Asignatura = {
            idTipoAsignatura : 0,
            Descripcion: "",
            opc: "new"
        };
        
    }
    
    obj.btnGuardarAsignatura= ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Asignatura: obj.Asignatura}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalAsignatura").modal("hide");
                    obj.getAsignaturas();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarAsignatura= ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Asignatura: obj.Asignatura}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalAsignatura").modal("hide");
                     obj.getAsignaturas();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalAsignatura").modal("show");
        obj.Asignatura = e;
        obj.Asignatura.opc = "edit";
        
    }
    
    obj.getAsignaturas= ()=>{
        obj.Asignatura = {
            idTipoAsignatura : 0,
            Descripcion: "",
            opc: "get"
        };
        
        $http({
            method: 'POST',
                url: url,
                data: {Asignatura: obj.Asignatura}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayAsignatura = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getAsignaturas();
     });
}
