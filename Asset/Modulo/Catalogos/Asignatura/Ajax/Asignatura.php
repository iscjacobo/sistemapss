<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Asignatura
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Asignatura {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        
        switch ($obj->Asignatura->opc){
            case 'new':
            case 'edit':
                if($this->setAsignaturas()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]= $obj->Asignatura->opc == 'new'? 
                            "El tipo Asignatura se registro satisfactoriamente":"El tipo Asignatura se edito satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $obj->Asignatura->opc == 'new'? 
                            "Error al registrar el tipo Asignatura":"Error al editar el tipo Asignatura";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getAsignaturas();
                break;
            
                
            
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setAsignaturas(){
       $obj = json_decode($this->formulario);
       switch ($obj->Asignatura->opc){
            case 'new':
                $sql = "INSERT INTO Catalogo_Tipo_Asignatura( idTipoAsignatura, Descripcion) "
                . "values (UPPER('{$obj->Asignatura->idTipoAsignatura}'),"
                    . "UPPER('{$obj->Asignatura->Descripcion}'))";
                break;
            case 'edit';
                $sql = "UPDATE Catalogo_Tipo_Asignatura SET idTipoAsignatura= UPPER('{$obj->Asignatura->idTipoAsignatura}'),"
                . "Descripcion = UPPER('{$obj->Asignatura->Descripcion}') where _id = {$obj->Asignatura->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getAsignaturas(){
        $array = array();
        $sql = "select * from Catalogo_Tipo_Asignatura";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Asignatura($array_principal);
$app->principal();
