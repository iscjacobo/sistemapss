/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * @author Manuel Jacobo
 */

var url = "./Modulo/Catalogos/Genero/Ajax/Genero.php"
tituloElectronico.controller('GeneroCtrl', ['$scope', '$http', GeneroCtrl]);

function GeneroCtrl($scope, $http) {
    var obj = $scope;
    obj.Genero = {};
    obj.arrayGeneros= [];

//Abrir el modal de nuevo
obj.btnNuevoGenero = () => {
    $("#ModalGenero").modal("show");
    obj.Genero = {};
    obj.Genero.opc = "new";
}
//Mandar a la base de datos para insertar
obj.btnGuardarGenero = () => {
    console.log(obj.Genero);
    $http({
        method: 'POST',
        url: url,
        data: {Genero: obj.Genero}
    }).then(function successCallback(res) {
        console.log(res.data);
        if (res.data.Bandera == 1) {
            toastr.success(res.data.mensaje);
            $("#ModalGenero").modal("hide");   
             obj.getGeneros();
        } else {
            toastr.error(res.data.mensaje);
        }
    }, function errorCallback(res) {
    });

}
//Buscar los cargos existentes en la base de datos
obj.getGeneros= ()=>{
    obj.Genero = {};
    obj.Genero.opc = "get";
    $http({
        method: 'POST',
            url: url,
            data: {Genero: obj.Genero}
        }).then(function successCallback(res){
            
            if(res.data.Bandera == 1){
                obj.arrayGeneros = res.data.Data;
                
            }else{
               console.log(res.data.mensaje);
            }
        }, function errorCallback(res){
            
    });
}

 angular.element(document).ready(function(){
     obj.getGeneros();
 });
 //Abrir el modal de editar
 obj.btnEditar = (e)=>{
    $("#ModalGenero").modal("show");
    obj.Genero = e;
    obj.Genero.opc = "edit";
}
 //Mandar a editar en la base de datos
 obj.btnEditarGenero = ()=>{
    $http({
         method: 'POST',
             url: url,
             data: {Genero : obj.Genero }
         }).then(function successCallback(res){
             console.log(res.data);
             if(res.data.Bandera == 1){
                 toastr.success(res.data.mensaje);
                 $("#ModalGenero").modal("hide");
                  obj.getGeneros();
             }else{
               toastr.error(res.data.mensaje);
             }
         }, function errorCallback(res){
             
     });
 }
}
