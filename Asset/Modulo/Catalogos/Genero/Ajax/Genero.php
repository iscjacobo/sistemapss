<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Daniel Vazquez
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Genero {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Genero->opc) {
            case 'new':
            case "edit":
                if ($this->setGeneros()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Genero->opc == 'new'? 
                    "El genero se registro satisfactoriamente":"El genero se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Genero->opc == 'new'? 
                    "Error al registrar Genero":"Error al editar Genero ";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getGeneros();
                break;
        }

        print json_encode($this->jsonData);
    }

    private function setGeneros() {
        $obj = json_decode($this->formulario);
        switch ($obj->Genero->opc) {
            case 'new':
                $sql = "INSERT INTO Catalogo_Genero(idGenero,Genero ) "
                        . "values (UPPER('{$obj->Genero->idGenero}'),UPPER('{$obj->Genero->Genero}'))";
                break;
            case 'edit';
                $sql = "UPDATE Catalogo_Genero SET idGenero =
                 UPPER('{$obj->Genero->idGenero}'), Genero = UPPER('{$obj->Genero->Genero}') where _id = {$obj->Genero->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getGeneros(){
        $array = array();
         $sql = "select * from Catalogo_Genero";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Genero($array_principal);
$app->principal();
