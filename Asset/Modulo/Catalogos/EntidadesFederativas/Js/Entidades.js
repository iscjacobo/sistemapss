/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/EntidadesFederativas/Ajax/Entidades.php"
tituloElectronico.controller('EntidadesCtrl',['$scope','$http',EntidadesCtrl]);

function EntidadesCtrl($scope,$http){
    var obj = $scope;
    obj.Entidad = {};
    obj.arrayEntidades = [];
    
    obj.btnNuevaEntidad = ()=>{
        $("#ModalEntidad").modal("show");
        obj.Entidad = {};
        obj.Entidad.opc="new";
    }
    
    obj.btnGuardarEntidad = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Entidad: obj.Entidad}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalEntidad").modal("hide");
                    obj.getEntidadesFederativas();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarEntidad = ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Entidad: obj.Entidad}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalEntidad").modal("hide");
                     obj.getEntidadesFederativas();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalEntidad").modal("show");
        obj.Entidad = e;
        obj.Entidad.opc = "edit";
        
    }
    
    obj.getEntidadesFederativas= ()=>{
        obj.Entidad = {};
        obj.Entidad.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Entidad: obj.Entidad}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayEntidades = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getEntidadesFederativas();
     });
}
