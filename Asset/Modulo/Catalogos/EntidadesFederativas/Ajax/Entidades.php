<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entidades
 *
 * @author francisco
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Entidades {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        
        switch ($obj->Entidad->opc){
            case 'new':
            case 'edit':
                if($this->setEntidades()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]= $obj->Entidad->opc == 'new'? 
                            "La Entidad Federativa se registro satisfactoriamente":"La Entidad Federativa se edito satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $obj->Entidad->opc == 'new'? 
                            "Error al registrar la Entidad Federativa":"Error al editar la Entidad Federativa";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getEntidades();
                break;
            
                
            
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setEntidades(){
       $obj = json_decode($this->formulario);
       switch ($obj->Entidad->opc){
            case 'new':
                 $sql = "INSERT INTO EntidadesFederativas( NOM_ENT, ENTIDAD_ABR, IDENTIDADFEDERATIVA) "
                . "values (UPPER('{$obj->Entidad->NOM_ENT}'),"
                    . "UPPER('{$obj->Entidad->ENTIDAD_ABR}'), "
                    . "'{$obj->Entidad->IDENTIDADFEDERATIVA}')";
                break;
            case 'edit';
                $sql = "UPDATE EntidadesFederativas SET NOM_ENT= UPPER('{$obj->Entidad->NOM_ENT}'),"
                . "ENTIDAD_ABR = UPPER('{$obj->Entidad->ENTIDAD_ABR}'), "
                . "IDENTIDADFEDERATIVA = '{$obj->Entidad->IDENTIDADFEDERATIVA}'"
                . " where _id = {$obj->Entidad->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getEntidades(){
        $array = array();
        $sql = "select * from EntidadesFederativas";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Entidades($array_principal);
$app->principal();
