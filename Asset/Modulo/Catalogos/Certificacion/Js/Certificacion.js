/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * @author Manuel Jacobo
 */

var url = "./Modulo/Catalogos/Certificacion/Ajax/Certificacion.php"
tituloElectronico.controller('CertificacionCtrl', ['$scope', '$http', CertificacionCtrl]);

function CertificacionCtrl($scope, $http) {
    var obj = $scope;
    obj.Certificacion = {};
    obj.arrayCertificaciones= [];


    //Abrir el modal de nuevo
    obj.btnNuevaCertificacion = () => {
        $("#ModalCertificacion").modal("show");
        obj.Certificacion = {};
        obj.Certificacion.opc = "new";
    }
    //Mandar a la base de datos para insertar
    obj.btnGuardarCertificacion = () => {
        console.log(obj.Certificacion);
        $http({
            method: 'POST',
            url: url,
            data: {Certificacion: obj.Certificacion}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                toastr.success(res.data.mensaje);
                $("#ModalCertificacion").modal("hide");   
                 obj.getCertificaciones();
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });

    }
    //Abrir el modal de editar
    obj.btnEditar = (e)=>{
        $("#ModalCertificacion").modal("show");
        obj.Certificacion = e;
        obj.Certificacion.opc = "edit";
    }
    //Mandar a editar en la base de datos
    obj.btnEditarCertificacion = ()=>{
        $http({
             method: 'POST',
                 url: url,
                 data: {Certificacion : obj.Certificacion }
             }).then(function successCallback(res){
                 console.log(res.data);
                 if(res.data.Bandera == 1){
                     toastr.success(res.data.mensaje);
                     $("#ModalCertificacion").modal("hide");
                      obj.getCertificaciones();
                 }else{
                   toastr.error(res.data.mensaje);
                 }
             }, function errorCallback(res){
                 
         });
     }
    //Buscar los cargos existentes en la base de datos
        obj.getCertificaciones= ()=>{
        obj.Certificacion = {};
        obj.Certificacion.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Certificacion: obj.Certificacion}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayCertificaciones = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getCertificaciones();
     });


}
