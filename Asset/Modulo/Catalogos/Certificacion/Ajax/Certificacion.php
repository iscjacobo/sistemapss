<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Certificacion {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Certificacion->opc) {
            case 'new':
            case "edit":
                if ($this->setCertificaciones()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Certificacion->opc == 'new'? 
                    "La Certificacion se registro satisfactoriamente":"La Certificacion se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Certificacion->opc == 'new'? 
                    "Error al registrar Certificacion":"Error al editar Certificacion ";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getCertificaciones();
                break;
        }

        print json_encode($this->jsonData);
    }

    private function setCertificaciones() {
        $obj = json_decode($this->formulario);
        switch ($obj->Certificacion->opc) {
            case 'new':
                $sql = "INSERT INTO Catalogo_Tipo_Certificacion(idTipoCertificacion, Tipo_Certificacion) "
                        . "values (UPPER('{$obj->Certificacion->idTipoCertificacion}'),
                                   UPPER('{$obj->Certificacion->Tipo_Certificacion}')
                        )";
                break;
            case 'edit';
                 $sql = "UPDATE Catalogo_Tipo_Certificacion SET 
                 idTipoCertificacion = UPPER('{$obj->Certificacion->idTipoCertificacion}') ,
                 Tipo_Certificacion = UPPER('{$obj->Certificacion->Tipo_Certificacion}') 
                 where _id = {$obj->Certificacion->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getCertificaciones(){
        $array = array();
         $sql = "select * from Catalogo_Tipo_Certificacion";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Certificacion($array_principal);
$app->principal();
