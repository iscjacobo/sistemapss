/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/Periodo/Ajax/Periodo.php"
tituloElectronico.controller('PeriodoCtrl',['$scope','$http',PeriodoCtrl]);

function PeriodoCtrl($scope,$http){
    var obj = $scope;
    obj.Periodo = {};
    obj.arrayPeriodos = [];
    
    obj.btnNuevoPeriodo = ()=>{
        $("#ModalPeriodo").modal("show");
        obj.Periodo = {};
        obj.Periodo.opc="new";
    }
    
    obj.btnGuardarPeriodo= ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Periodo: obj.Periodo}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalPeriodo").modal("hide");
                    obj.getPeriodos();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarPeriodo= ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Periodo: obj.Periodo}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalPeriodo").modal("hide");
                     obj.getPeriodos();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalPeriodo").modal("show");
        obj.Periodo = e;
        obj.Periodo.opc = "edit";
        
    }
    
    obj.getPeriodos= ()=>{
        obj.Periodo = {};
        obj.Periodo.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Periodo: obj.Periodo}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayPeriodos = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getPeriodos();
     });
}
