<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entidades
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Periodo {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        
        switch ($obj->Periodo->opc){
            case 'new':
            case 'edit':
                if($this->setPeriodos()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]= $obj->Periodo->opc == 'new'? 
                            "El tipo Periodo se registro satisfactoriamente":"El tipo Periodo se edito satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $obj->Periodo->opc == 'new'? 
                            "Error al registrar el tipo Periodo":"Error al editar el tipo Periodo";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getPeriodos();
                break;
            
                
            
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setPeriodos(){
       $obj = json_decode($this->formulario);
       switch ($obj->Periodo->opc){
            case 'new':
                 $sql = "INSERT INTO Catalogo_tipo_periodo( idTipoPeriodo, Tipo_Periodo) "
                . "values (UPPER('{$obj->Periodo->idTipoPeriodo}'),"
                    . "UPPER('{$obj->Periodo->Tipo_Periodo}'))";
                break;
            case 'edit';
                $sql = "UPDATE Catalogo_tipo_periodo SET idTipoPeriodo= UPPER('{$obj->Periodo->idTipoPeriodo}'),"
                . "Tipo_Periodo = UPPER('{$obj->Periodo->Tipo_Periodo}') where _id = {$obj->Periodo->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getPeriodos(){
        $array = array();
        $sql = "select * from Catalogo_tipo_periodo";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Periodo($array_principal);
$app->principal();
