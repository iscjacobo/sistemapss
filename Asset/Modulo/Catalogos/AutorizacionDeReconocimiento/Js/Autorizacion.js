/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/AutorizacionDeReconocimiento/Ajax/Autorizacion.php"
tituloElectronico.controller('AutorizacionCtrl',['$scope','$http',AutorizacionCtrl]);

function AutorizacionCtrl($scope,$http){
    var obj = $scope;
    obj.Autorizacion = {};
    obj.arrayAutorizaciones = [];
    
    obj.btnNuevaAutorizacion = ()=>{
        $("#ModalAutorizacion").modal("show");
        obj.Autorizacion = {};
        obj.Autorizacion.opc="new";
    }
    
    obj.btnGuardarAutorizacion = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Autorizacion: obj.Autorizacion}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalAutorizacion").modal("hide");
                    obj.getAutorizaciones();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarAutorizacion = ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Autorizacion: obj.Autorizacion}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalAutorizacion").modal("hide");
                     obj.getAutorizaciones();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalAutorizacion").modal("show");
        obj.Autorizacion = e;
        obj.Autorizacion.opc = "edit";
        
    }
    
    obj.getAutorizaciones= ()=>{
        obj.Autorizacion = {};
        obj.Autorizacion.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Autorizacion: obj.Autorizacion}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayAutorizaciones = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getAutorizaciones();
     });
}
