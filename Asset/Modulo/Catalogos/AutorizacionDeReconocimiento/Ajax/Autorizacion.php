<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entidades
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Autorizacion {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        
        switch ($obj->Autorizacion->opc){
            case 'new':
            case 'edit':
                if($this->setAutorizacion()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]= $obj->Autorizacion->opc == 'new'? 
                            "La Autorizacion de reconocimiento se registro satisfactoriamente":"La autorizacion de reconocimiento se edito satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $obj->Autorizacion->opc == 'new'? 
                            "Error al registrar la autorizacion de reconocimiento":"Error al editar la autorizacion de reconocimiento";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getAutorizacion();
                break;
            
                
            
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setAutorizacion(){
       $obj = json_decode($this->formulario);
       switch ($obj->Autorizacion->opc){
            case 'new':
                $sql = "INSERT INTO Autorizacion_reconocimiento(Autorizacion_reconocimiento) "
                        . "values (UPPER('{$obj->Autorizacion->Autorizacion_reconocimiento}'))";
                break;
            case 'edit';
                $sql = "UPDATE Autorizacion_reconocimiento SET 
                       Autorizacion_reconocimiento  = UPPER('{$obj->Autorizacion->Autorizacion_reconocimiento}')              
                         where _id = {$obj->Autorizacion->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getAutorizacion(){
        $array = array();
        $sql = "select * from Autorizacion_reconocimiento";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Autorizacion($array_principal);
$app->principal();
