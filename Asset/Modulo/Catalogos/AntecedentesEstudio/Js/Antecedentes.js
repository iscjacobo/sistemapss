var url = "./Modulo/Catalogos/AntecedentesEstudio/Ajax/Antecedentes.php"
tituloElectronico.controller('NivelEstudiosCtrl', ['$scope', '$http', NivelEstudiosCtrl]);

function NivelEstudiosCtrl($scope, $http) {

    var obj = $scope;
    obj.NivelEstudio = {};
    obj.arrayNivelEstudios = [];
    obj.arrayTipo_documento = [];

//Funcion para abrir modal de nuevo nivel
    obj.btnNuevoNivel = () => {
        $("#ModalNivelEstudios").modal("show");
        obj.NivelEstudio = {};
        obj.NivelEstudio.opc = "new";
    };

//Funcion para mandar informacion al servidor
obj.btnGuardarNivel = () => {
    console.log(obj.NivelEstudio);
    $http({
        method: 'POST',
        url: url,
        data: {NivelEstudio: obj.NivelEstudio}
    }).then(function successCallback(res) {
        console.log(res.data);
        if (res.data.Bandera == 1) {
            toastr.success(res.data.mensaje);
            $("#ModalNivelEstudios").modal("hide");   
             obj.getNivelEstudio();
        } else {
            toastr.error(res.data.mensaje);
        }
    }, function errorCallback(res) {
    });

}


    //Abrir el modal de editar
    obj.btnEditar = (e)=>{
        $("#ModalNivelEstudios").modal("show");
        obj.NivelEstudio = e;
        obj.NivelEstudio.opc = "edit";
    }
    
    obj.getDocumento = (e)=>{
        var id = obj.arrayTipo_documento.find(a => a._id === e)
        return id.Documento;
    }
    
    //Mandar a editar en la base de datos
    obj.btnEditarNivel = ()=>{
        $http({
             method: 'POST',
                 url: url,
                 data: {NivelEstudio : obj.NivelEstudio }
             }).then(function successCallback(res){
                 console.log(res.data);
                 if(res.data.Bandera == 1){
                     toastr.success(res.data.mensaje);
                     $("#ModalNivelEstudios").modal("hide");
                      obj.getNivelEstudio();
                 }else{
                   toastr.error(res.data.mensaje);
                 }
             }, function errorCallback(res){
                 
         });
     }
    //Buscar los cargos existentes en la base de datos
        obj.getNivelEstudio= ()=>{
            obj.NivelEstudio = {};
            obj.NivelEstudio.opc = "get";
            $http({
                method: 'POST',
                    url: url,
                    data: {NivelEstudio: obj.NivelEstudio}
                }).then(function successCallback(res){

                    if(res.data.Bandera == 1){
                        obj.arrayNivelEstudios = res.data.Data;

                    }else{
                       console.log(res.data.mensaje);
                    }
                }, function errorCallback(res){
                
            });
        }
        
        obj.getTipo_documento = ()=>{
            obj.NivelEstudio = {};
            obj.NivelEstudio.opc = "doc";
            $http({
                method: 'POST',
                    url: url,
                    data: {NivelEstudio: obj.NivelEstudio}
                }).then(function successCallback(res){

                    if(res.data.Bandera == 1){
                        obj.arrayTipo_documento = res.data.Data;

                    }else{
                       console.log(res.data.mensaje);
                    }
                }, function errorCallback(res){
                
            });
        }
    
     angular.element(document).ready(function(){
         obj.getNivelEstudio();
         obj.getTipo_documento();
     });



}


