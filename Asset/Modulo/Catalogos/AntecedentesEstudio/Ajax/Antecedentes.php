<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Niveles {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->NivelEstudio->opc) {
            case 'new':
            case "edit":
                if ($this->setNiveles()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->NivelEstudio->opc == 'new'? 
                    "El antecedente se registro satisfactoriamente":"El antecedente se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->NivelEstudio->opc == 'new'? 
                    "Error al registrar el antecedente":"Error al editar el antecedente";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getNiveles();
                break;
            case 'doc':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getTipo_Documentos();
                break;
                
        }

        print json_encode($this->jsonData);
    }

    private function setNiveles() {
        $obj = json_decode($this->formulario);
        switch ($obj->NivelEstudio->opc) {
            case 'new':
                $sql = "INSERT INTO Estudio_antecedentes(Tipo_estudio_antecedente, Tipo_educativo_del_antecedente,  _IdEstudios) "
                        . "values (UPPER('{$obj->NivelEstudio->Tipo_estudio_antecedente}'), 
                                   UPPER('{$obj->NivelEstudio->Tipo_educativo_del_antecedente}'),
                                         '{$obj->NivelEstudio->_IdEstudios}')";
                break;
            case 'edit';
                 $sql = "UPDATE Estudio_antecedentes SET 
                 Tipo_estudio_antecedente= UPPER('{$obj->NivelEstudio->Tipo_estudio_antecedente}'),
                 Tipo_educativo_del_antecedente= UPPER('{$obj->NivelEstudio->Tipo_educativo_del_antecedente}'),
                 _IdEstudios= '{$obj->NivelEstudio->_IdEstudios}'
                 where _id = {$obj->NivelEstudio->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getNiveles(){
        $array = array();
         $sql = "select * from Estudio_antecedentes";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    
    private function getTipo_Documentos(){
        $array = array();
        $sql = "SELECT * FROM Tipo_documento where Estatus = 1";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Niveles($array_principal);
$app->principal();
