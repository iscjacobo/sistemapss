/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * @author Manuel Jacobo
 */

var url = "./Modulo/Catalogos/FundamentoLegal/Ajax/FundamentoLegal.php"
tituloElectronico.controller('FundamentosCtrl', ['$scope', '$http', FundamentosCtrl]);

function FundamentosCtrl($scope, $http) {
    var obj = $scope;
    obj.Fundamento = {};
    obj.arrayFundamentos= [];


    //Abrir el modal de nuevo
    obj.btnNuevoFundamento = () => {
        $("#ModalFundamento").modal("show");
        obj.Fundamento = {};
        obj.Fundamento.opc = "new";
    }
    //Mandar a la base de datos para insertar
    obj.btnGuardarFundamento = () => {
        console.log(obj.Fundamento);
        $http({
            method: 'POST',
            url: url,
            data: {Fundamento: obj.Fundamento}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                toastr.success(res.data.mensaje);
                $("#ModalFundamento").modal("hide");   
                 obj.getFundamentoslegales();
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });

    }
    //Abrir el modal de editar
    obj.btnEditar = (e)=>{
        $("#ModalFundamento").modal("show");
        obj.Fundamento = e;
        obj.Fundamento.opc = "edit";
    }
    //Mandar a editar en la base de datos
    obj.btnEditarFundamento = ()=>{
        $http({
             method: 'POST',
                 url: url,
                 data: {Fundamento : obj.Fundamento }
             }).then(function successCallback(res){
                 console.log(res.data);
                 if(res.data.Bandera == 1){
                     toastr.success(res.data.mensaje);
                     $("#ModalFundamento").modal("hide");
                      obj.getFundamentoslegales();
                 }else{
                   toastr.error(res.data.mensaje);
                 }
             }, function errorCallback(res){
                 
         });
     }
    //Buscar los cargos existentes en la base de datos
        obj.getFundamentoslegales= ()=>{
        obj.Fundamento = {};
        obj.Fundamento.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Fundamento: obj.Fundamento}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayFundamentos = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getFundamentoslegales();
     });


}
