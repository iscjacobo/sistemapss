<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Fundamento {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Fundamento->opc) {
            case 'new':
            case "edit":
                if ($this->setFundamentos()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Fundamento->opc == 'new'? 
                    "El Fundamento registro satisfactoriamente":"El Fundamento se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Fundamento->opc == 'new'? 
                    "Error al registrar Fundamento":"Error al editar Fundamento ";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getFundamentos();
                break;
        }

        print json_encode($this->jsonData);
    }

    private function setFundamentos() {
        $obj = json_decode($this->formulario);
        switch ($obj->Fundamento->opc) {
            case 'new':
                $sql = "INSERT INTO Fundamento_legal_servicio_social(Fundamento_legal_servicio_social) "
                        . "values (UPPER('{$obj->Fundamento->Fundamento_legal_servicio_social}'))";
                break;
            case 'edit';
                $sql = "UPDATE Fundamento_legal_servicio_social SET 
                Fundamento_legal_servicio_social =
                UPPER('{$obj->Fundamento->Fundamento_legal_servicio_social}')
                where _id = {$obj->Fundamento->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getFundamentos(){
        $array = array();
         $sql = "select * from Fundamento_legal_servicio_social";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Fundamento($array_principal);
$app->principal();
