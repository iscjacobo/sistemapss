<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observaciones
 * 
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Observaciones {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        
        switch ($obj->Observaciones->opc){
            case 'new':
            case 'edit':
                if($this->setObservaciones()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]= $obj->Observaciones->opc == 'new'? 
                            "Laas Observaciones se registraron satisfactoriamente":"Las Observaciones se editaron satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $obj->Observaciones->opc == 'new'? 
                            "Error al registrar las Observacione":"Error al editar las Observaciones";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getObservaciones();
                break;
            
                
            
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setObservaciones(){
       $obj = json_decode($this->formulario);
       switch ($obj->Observaciones->opc){
            case 'new':
                $sql = "INSERT INTO Catalogo_Observaciones(idObservaciones, Descripcion, Descripcion_Corta) values (
                UPPER('{$obj->Observaciones->idObservaciones}'),
                UPPER('{$obj->Observaciones->Descripcion}'),
                UPPER('{$obj->Observaciones->Descripcion_Corta}')
                )";
                break;
            case 'edit';
                $sql = "UPDATE Catalogo_Observaciones SET 
                 idObservaciones    = UPPER('{$obj->Observaciones->idObservaciones}'),
                 Descripcion        = UPPER('{$obj->Observaciones->Descripcion}'),
                 Descripcion_Corta  = UPPER('{$obj->Observaciones->Descripcion_Corta}')
                 where _id = {$obj->Observaciones->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getObservaciones(){
        $array = array();
        $sql = "select * from Catalogo_Observaciones";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Observaciones($array_principal);
$app->principal();
