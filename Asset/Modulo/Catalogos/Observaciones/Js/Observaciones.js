/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/Observaciones/Ajax/Observaciones.php"
tituloElectronico.controller('ObservacionesCtrl',['$scope','$http',ObservacionesCtrl]);

function ObservacionesCtrl($scope,$http){
    var obj = $scope;
    obj.Observaciones = {};
    obj.arrayObservaciones = [];
    
    obj.btnNuevaObservaciones = ()=>{
        $("#ModalObservaciones").modal("show");
        obj.Observaciones = {};
        obj.Observaciones.opc="new";
    }
    
    obj.btnGuardarObservaciones = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Observaciones: obj.Observaciones}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalObservaciones").modal("hide");
                    obj.getObservaciones();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarObservaciones= ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Observaciones: obj.Observaciones}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalObservaciones").modal("hide");
                     obj.getObservaciones();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalObservaciones").modal("show");
        obj.Observaciones = e;
        obj.Observaciones.opc = "edit";
        
    }
    
    obj.getObservaciones= ()=>{
        obj.Observaciones = {};
        obj.Observaciones.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Observaciones: obj.Observaciones}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayObservaciones = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getObservaciones();
     });
}
