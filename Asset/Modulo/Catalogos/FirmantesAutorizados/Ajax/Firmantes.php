<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Firmantes {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Firmante->opc) {
            case 'new':
            case "edit":
                if ($this->setFirmantes()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Firmante->opc == 'new'? 
                    "El cargo de firmante se registro satisfactoriamente":"El cargo de firmante se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Firmante->opc == 'new'? 
                    "Error al registrar El cargo de firmante":"Error al editar El cargo de firmante";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getFirmantes();
                break;
        }

        print json_encode($this->jsonData);
    }

    private function setFirmantes() {
        $obj = json_decode($this->formulario);
        switch ($obj->Firmante->opc) {
            case 'new':
                $sql = "INSERT INTO Cargo_firmantes_autorizados(Cargo_firmante) "
                        . "values (UPPER('{$obj->Firmante->Cargo_firmante}'))";
                break;
            case 'edit';
                $sql = "UPDATE Cargo_firmantes_autorizados SET Cargo_firmante= UPPER('{$obj->Firmante->Cargo_firmante}') where _id = {$obj->Firmante->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getFirmantes(){
        $array = array();
         $sql = "select * from Cargo_firmantes_autorizados";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Firmantes($array_principal);
$app->principal();
