/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 * @author Manuel Jacobo
 */

var url = "./Modulo/Catalogos/FirmantesAutorizados/Ajax/Firmantes.php"
tituloElectronico.controller('FirmantesCtrl', ['$scope', '$http', FirmantesCtrl]);

function FirmantesCtrl($scope, $http) {
    var obj = $scope;
    obj.Firmante = {};
    obj.arrayFirmantes = [];


    //Abrir el modal de nuevo
    obj.btnNuevoFirmante = () => {
        $("#ModalFirmante").modal("show");
        obj.Firmante = {};
        obj.Firmante.opc = "new";
    }
    //Mandar a la base de datos para insertar
    obj.btnGuardarFirmante = () => {
        console.log(obj.Firmante);
        $http({
            method: 'POST',
            url: url,
            data: {Firmante: obj.Firmante}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                toastr.success(res.data.mensaje);
                $("#ModalFirmante").modal("hide");   
                 obj.getFirmantesAutorizados();
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });

    }
    //Abrir el modal de editar
    obj.btnEditar = (e)=>{
        $("#ModalFirmante").modal("show");
        obj.Firmante = e;
        obj.Firmante.opc = "edit";
    }
    //Mandar a editar en la base de datos
    obj.btnEditarFirmante = ()=>{
        $http({
             method: 'POST',
                 url: url,
                 data: {Firmante : obj.Firmante }
             }).then(function successCallback(res){
                 console.log(res.data);
                 if(res.data.Bandera == 1){
                     toastr.success(res.data.mensaje);
                     $("#ModalFirmante").modal("hide");
                      obj.getFirmantesAutorizados();
                 }else{
                   toastr.error(res.data.mensaje);
                 }
             }, function errorCallback(res){
                 
         });
     }
    //Buscar los cargos existentes en la base de datos
        obj.getFirmantesAutorizados= ()=>{
        obj.Firmante = {};
        obj.Firmante.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Firmante: obj.Firmante}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayFirmantes = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getFirmantesAutorizados();
     });


}
