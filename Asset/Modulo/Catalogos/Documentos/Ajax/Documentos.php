<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Documento
 *
 * @author francisco
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Documento {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }
    
    public function main(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch($this->formulario->Documento->opc){
            case 'new':
            case 'edit':
            case 'delete':
           
                if($this->setDocumentos()){
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] = "El tipo de documento se ha ingresado satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] = "Error: al ingresar el documento";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getDocumentos();
                break;
        }
        print json_encode($this->jsonData);
    }
    
    private function setDocumentos(){
        $fecha = date("Y-m-d");
        switch ($this->formulario->Documento->opc){
            case 'new';
                $sql = "INSERT INTO Tipo_documento(Documento, Estatus, USRCreacion, "
                        . "FechaCreacion, USRModificacion, FechaModificacion) values ("
                        . "'{$this->formulario->Documento->Documento}',1,'{$_SESSION["usr"]}','$fecha','{$_SESSION["usr"]}','$fecha')";
                break;
            case 'edit':
                $sql = "UPDATE Tipo_documento set Documento = '{$this->formulario->Documento->Documento}',"
                . "USRModificacion = '{$_SESSION["usr"]}', FechaModificacion = '$fecha'"
                        . " where _id = {$this->formulario->Documento->_id}";
                break;
            case 'delete':
                $sql = "UPDATE Tipo_documento SET Estatus = 0 where _id = {$this->formulario->Documento->_id} ";
                break;
                
        }
        return $this->conn->query($sql)? true:false;
    }
    
    private function getDocumentos(){
        $array = array();
        $historico = $this->formulario->Documento->historico == "true" ? 0:1;
        $sql = "SELECT * FROM Tipo_documento where Estatus = $historico";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch()){
            array_push($array, $row);
        }
        return $array;
    }
    
}

$app = new Documento ($array_principal);
$app->main();
