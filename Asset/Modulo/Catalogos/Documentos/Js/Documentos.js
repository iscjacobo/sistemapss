/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/Documentos/Ajax/Documentos.php"
tituloElectronico.controller('DocumentosCtrl', ['$scope', '$http', DocumentosCtrl]);

function DocumentosCtrl($scope,$http){
    var obj = $scope;
    obj.arrayDocumentos = [];
    obj.Documento = {
        Documento:"",
        opc:""
    };
    obj.historico = false;
    
    obj.getDocumentos = ()=>{
        obj.Documento.opc = "get";
        obj.Documento.historico = obj.historico;
        $http({
            method: 'POST',
            url: url,
            data: {Documento: obj.Documento}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                obj.arrayDocumentos = res.data.Data;
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    
    
    obj.btnNuevoDocumento = ()=>{
        obj.Documento.Documento = "";
        obj.Documento.opc = "new";
        $("#ModalDocumento").modal("show");
    }
    
    obj.btnGuardarDocumento = ()=>{
        $http({
            method: 'POST',
            url: url,
            data: {Documento: obj.Documento}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                $("#ModalDocumento").modal("hide");   
                toastr.success(res.data.mensaje);
                 obj.getDocumentos();
                 
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    
    obj.btnEditar = (e)=>{
        obj.Documento = e;
        obj.Documento.opc = "edit";
        $("#ModalDocumento").modal("show");
    }
    
    obj.btnBaja = (e)=>{
        if(confirm("Estas seguro de dar de baja el documento")){
            obj.Documento = e;
            obj.Documento.opc = "delete";
            $http({
                method: 'POST',
                url: url,
                data: {Documento: obj.Documento}
            }).then(function successCallback(res) {
                console.log(res.data);
                if (res.data.Bandera == 1) {
                    $("#ModalDocumento").modal("hide");   
                    toastr.success(res.data.mensaje);
                     obj.getDocumentos();

                } else {
                    toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res) {
            });
        }
        
    }
    obj.btnEditarDocumento = ()=>{
        $http({
            method: 'POST',
            url: url,
            data: {Documento: obj.Documento}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                $("#ModalDocumento").modal("hide");   
                toastr.success(res.data.mensaje);
                 obj.getDocumentos();
                 
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    
    angular.element(document).ready(function(){
         obj.getDocumentos();
     });
    
    
}

