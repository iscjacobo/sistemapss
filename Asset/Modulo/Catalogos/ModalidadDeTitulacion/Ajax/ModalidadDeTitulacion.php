<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Modalidad {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Modalidad->opc) {
            case 'new':
            case "edit":
                if ($this->setModalidad()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Modalidad->opc == 'new'? 
                    "El nivel de estudio se registro satisfactoriamente":"El  nivel de estudio se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Modalidad->opc == 'new'? 
                    "Error al registrar nivel de estudio":"Error al editar  nivel de estudio";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getModalidad();
                break;
       
                
        }

        print json_encode($this->jsonData);
    }

    private function setModalidad() {
        $obj = json_decode($this->formulario);
        switch ($obj->Modalidad->opc) {
            case 'new':
           $sql = "INSERT INTO Modalidad_de_titulacion(Modalidad_titulacion, Tipo_modalidad) "
                        . "values (UPPER('{$obj->Modalidad->Modalidad_titulacion}'), 
                                   UPPER('{$obj->Modalidad->Tipo_modalidad}'))";
                break;
            case 'edit';
                 $sql = "UPDATE Modalidad_de_titulacion SET 
                 Modalidad_titulacion= UPPER('{$obj->Modalidad->Tipo_estudio_antecedente}'),
                 Tipo_modalida= UPPER('{$obj->Modalidad->Tipo_educativo_del_antecedente}')
                 where _id = {$obj->Modalidad->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getModalidad(){
        $array = array();
         $sql = "select * from Modalidad_de_titulacion";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    
  

}

$app = new Modalidad($array_principal);
$app->principal();
