var url = "./Modulo/Catalogos/ModalidadDeTitulacion/Ajax/ModalidadDeTitulacion.php"
tituloElectronico.controller('ModalidadCtrl', ['$scope', '$http', ModalidadCtrl]);

function ModalidadCtrl($scope, $http) {

    var obj = $scope;
    obj.Modalidad = {};
    obj.arrayModalidades = [];

//Funcion para abrir modal de Modalidad
    obj.btnNuevaModalidad = () => {
        $("#ModalModalidad").modal("show");
        obj.Modalidad = {};
        obj.Modalidad.opc = "new";
    };

//Funcion para mandar informacion al servidor
obj.btnGuardarModalidad = () => {
    console.log(obj.Modalidad);
    $http({
        method: 'POST',
        url: url,
        data: {Modalidad: obj.Modalidad}
    }).then(function successCallback(res) {
        console.log(res.data);
        if (res.data.Bandera == 1) {
            toastr.success(res.data.mensaje);
            $("#ModalModalidad").modal("hide");   
             obj.getModalidad();
        } else {
            toastr.error(res.data.mensaje);
        }
    }, function errorCallback(res) {
    });

}


    //Abrir el modal de editar
    obj.btnEditar = (e)=>{
        $("#ModalModalidad").modal("show");
        obj.Modalidad = e;
        obj.Modalidad.opc = "edit";
    }
    
  
    //Mandar a editar en la base de datos
    obj.btnEditarModalidad = ()=>{
        $http({
             method: 'POST',
                 url: url,
                 data: {Modalidad : obj.Modalidad }
             }).then(function successCallback(res){
                 console.log(res.data);
                 if(res.data.Bandera == 1){
                     toastr.success(res.data.mensaje);
                     $("#ModalModalidad").modal("hide");
                      obj.getModalidad();
                 }else{
                   toastr.error(res.data.mensaje);
                 }
             }, function errorCallback(res){
                 
         });
     }
    //Buscar los cargos existentes en la base de datos
        obj.getModalidad= ()=>{
            obj.Modalidad = {};
            obj.Modalidad.opc = "get";
            $http({
                method: 'POST',
                    url: url,
                    data: {Modalidad: obj.Modalidad}
                }).then(function successCallback(res){

                    if(res.data.Bandera == 1){
                        obj.arrayModalidades = res.data.Data;

                    }else{
                       console.log(res.data.mensaje);
                    }
                }, function errorCallback(res){
                
            });
        }
  
     angular.element(document).ready(function(){
         obj.getModalidad();
     });



}


