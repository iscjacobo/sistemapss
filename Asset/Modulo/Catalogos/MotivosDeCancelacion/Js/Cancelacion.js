/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Catalogos/MotivosDeCancelacion/Ajax/Cancelacion.php"
tituloElectronico.controller('CancelacionCtrl',['$scope','$http',CancelacionCtrl]);

function CancelacionCtrl($scope,$http){
    var obj = $scope;
    obj.Cancelacion = {};
    obj.Cancelaciones = [];
    
    obj.btnNuevaCancelacion = ()=>{
        $("#ModalCancelacion").modal("show");
        obj.Cancelacion = {};
        obj.Cancelacion.opc="new";
    }
    
    obj.btnGuardarCancelacion = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Cancelacion: obj.Cancelacion}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalCancelacion").modal("hide");
                    obj.getCancelaciones();
                }else{
                   toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
        
    }
    
    obj.btnEditarCancelacion= ()=>{
       $http({
            method: 'POST',
                url: url,
                data: {Cancelacion: obj.Cancelacion}
            }).then(function successCallback(res){
                console.log(res.data);
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);
                    $("#ModalCancelacion").modal("hide");
                     obj.getCancelaciones();
                }else{
                  toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnEditar = (e)=>{
        $("#ModalCancelacion").modal("show");
        obj.Cancelacion = e;
        obj.Cancelacion.opc = "edit";
        
    }
    
    obj.getCancelaciones= ()=>{
        obj.Cancelacion = {};
        obj.Cancelacion.opc = "get";
        $http({
            method: 'POST',
                url: url,
                data: {Cancelacion: obj.Cancelacion}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayCancelaciones = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
     angular.element(document).ready(function(){
         obj.getCancelaciones();
     });
}
