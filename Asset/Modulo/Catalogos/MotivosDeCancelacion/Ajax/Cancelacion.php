<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Firmantes
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Cancelacion {

    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera" => 0, "mensaje" => ""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase

    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }

    public function __destruct() {
        unset($this->conn);
    }

    public function principal() {
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);

        switch ($obj->Cancelacion->opc) {
            case 'new':
            case "edit":
                if ($this->setCancelaciones()) {
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] =  $obj->Cancelacion->opc == 'new'? 
                    "El motivo se registro satisfactoriamente":"El motivo se edito satisfactoriamente";
                } else {
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] =  $obj->Cancelacion->opc == 'new'? 
                    "Error al registrar motivo":"Error al editar motivo ";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getCancelaciones();
                break;
        }

        print json_encode($this->jsonData);
    }

    private function setCancelaciones() {
        $obj = json_decode($this->formulario);
        switch ($obj->Cancelacion->opc) {
            case 'new':
                     $sql = "INSERT INTO Motivos_cancelacion(Id_motivo_cancelacion, Descripcion_cancelacion) "
                        . "values (UPPER('{$obj->Cancelacion->Id_motivo_cancelacion}'),"
                        . "UPPER('{$obj->Cancelacion->Descripcion_cancelacion}')"
                        .")";
                break;
            case 'edit';
                  $sql = "UPDATE Motivos_cancelacion SET 
                 Id_motivo_cancelacion =
                 UPPER('{$obj->Cancelacion->Id_motivo_cancelacion}'),
                 Descripcion_cancelacion =
                 UPPER('{$obj->Cancelacion->Descripcion_cancelacion}')
                  where _id = {$obj->Cancelacion->_id}";
                break;
        }
        return $this->conn->query($sql);
    }
    
    private function getCancelaciones(){
        $array = array();
         $sql = "select * from Motivos_cancelacion";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

}

$app = new Cancelacion($array_principal);
$app->principal();
