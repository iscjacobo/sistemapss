/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var url = "./Modulo/Configuracion/Usuarios/Ajax/Usuarios.php";
tituloElectronico
        .controller('UsuariosCtrl',['$scope','$http',UsuariosCtrl])
        .controller('UsuariosNewCtrl',['$scope','$http',UsuariosNewCtrl])
        .controller('UsuariosEditCtrl',['$scope','$http',UsuariosEditCtrl]);

function UsuariosCtrl($scope,$http){
    var obj = $scope;
    obj.arrayInstituciones =[];
    obj.arrayUsuarios = [];
    obj.Institucion={};
    obj.rol="";
    obj.btnShow = false;
    obj.arrayTipo_usuario = [
        {valor:"Admin", descripcion:"Administrador", rol:"root"},
        {valor:"CtrlEsc", descripcion:"Control Escolar", rol:"Admin"},
    ]
    
    obj.Sucursal = {
        _idInstitucion:"",
        opc:"" 
    };
    obj.historico = false; 
     
    obj.btnNuevoUsuario = ()=>{
        window.location.href = "?mod=Usuarios&opc=nuevo&id="+obj.Institucion.id;
    };
    
    obj.btnGetUsuarios  = ()=>{
        obj.btnShow = true;
        obj.Sucursal._idInstitucion = obj.Institucion.id;
        obj.Sucursal.opc = "get"
        $http({
            method: 'POST',
                url: url,
                data: {Usuarios: obj.Sucursal}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                   obj.arrayUsuarios = res.data.Data;                   
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.getInstituciones = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Usuarios: {opc: obj.rol=="root"? "institucion":"sucursales"}}
            }).then(function successCallback(res){
                
                if(res.data.Bandera == 1){
                    obj.arrayInstituciones = res.data.Data;
                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.getTipoUsuario = (tipo)=>{
        var id = obj.arrayTipo_usuario.find(e=>e.valor === tipo);
        return id.descripcion;
    }
    
    obj.btnEliminarUsuario = (id)=>{
        if(confirm("¿Deseas dar de baja al usuario?")){
            var Usuario = {
                _id : id,
                opc:"delete"
            }
            $http({
                method: 'POST',
                    url: url,
                    data: {Usuarios: Usuario}
                }).then(function successCallback(res){
                    if(res.data.Bandera == 1){
                        obj.getInstituciones();
                        toastr.success(res.data.mensaje);
                    }else{
                       toastr.error(res.data.mensaje);
                    }
                }, function errorCallback(res){
                
            });
        }
    }
    
    obj.btnEditarUsuario = (id)=>{
         window.location.href = "?mod=Usuarios&opc=editar&id="+id;
    }
     angular.element(document).ready(function(){
         obj.getInstituciones();
         console.log(obj.rol);
     });
};

function UsuariosNewCtrl($scope,$http){
    var obj = $scope;
    obj.idCampus;
    obj.formEnabled = false;
    obj.rol="";
    obj.arrayTipo_usuario = [
        {valor:"Admin", descripcion:"Administrador", rol:"root"},
        {valor:"CtrlEsc", descripcion:"Control Escolar", rol:"Admin"},
    ]
    obj.Usuario = {
        Nombre: "",
        Ap_Paterno:"",
        Ap_Materno:"",
        email:"",
        rol:"",
        username:"",
        _idInstitucion:0,
        _idCampus:0,
        opc: "new"
    }
    
    obj.btnCrearUsuario = ()=>{
        if(obj.rol === "root"){
            obj.Usuario._idInstitucion = obj.idCampus;
        }else if(obj.rol === "Admin"){
            obj.Usuario._idCampus = obj.idCampus;
        }
        
        $http({
            method: 'POST',
                url: url,
                data: {Usuarios: obj.Usuario}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.formEnabled = true;
                    obj.Usuario.username= res.data.username;
                    obj.Usuario.password = res.data.Password;
                    console.log(obj.Usuario);
                    $("#Moduserpass").modal("show");
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnNuevoUsuario = ()=>{
       obj.Usuario = {
            Nombre: "",
            Ap_Paterno:"",
            Ap_Materno:"",
            email:"",
            rol:"",
            username:"",
            _idInstitucion:0,
            _idCampus:0,
            opc: "new"
        } 
        obj.formEnabled = false;
    }
    
    obj.btnRegresar = ()=>{
        location.href="?mod=Usuarios";
    }
    
    obj.btnShowmodal = ()=>{
        $("#Moduserpass").modal("show");
    }
    
}

function UsuariosEditCtrl($scope,$http){
    var obj = $scope;
    obj.USR = {
        _idUsr:0,
        opc:"getOne"
    };
    obj.Usuario = {};
    console.log(obj.USR);
    obj.formEnabled = true;
    obj.rol="";
    obj.arrayTipo_usuario = [
        {valor:"Admin", descripcion:"Administrador", rol:"root"},
        {valor:"CtrlEsc", descripcion:"Control Escolar", rol:"Admin"},
    ]
    
    obj.btnEditarUsuario = ()=>{
        console.log("entro al boton editar",obj.formEnabled);
        obj.formEnabled = false;
        console.log(obj.formEnabled);
    }
        
    obj.btnGuardarUsuario = ()=>{
       
        $http({
            method: 'POST',
                url: url,
                data: {Usuarios: obj.Usuario}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.formEnabled = true;                    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
        
    obj.getOneUsuario = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Usuarios: obj.USR}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Usuario = res.data.Data;
                    obj.Usuario.opc = "edit";
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
    
    obj.btnRegresar = ()=>{
        location.href="?mod=Usuarios";
    }
    
    angular.element(document).ready(function(){
        obj.getOneUsuario();
        console.log(obj.USR);
     });
}