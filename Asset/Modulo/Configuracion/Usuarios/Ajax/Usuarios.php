<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuarios
 *
 * @author francisco
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');
class Usuarios {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    private $pass;
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function main(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch($this->formulario->Usuarios->opc){
            case 'new':
                $this->jsonData["username"] = $this->formulario->Usuarios->email;
                $this->jsonData["Password"] = $this->getPassword();
                if($this->setUsuarios()){
                    $this->formulario->Usuarios->last_id = $this->conn->last_id();
                    if($this->setSeguridad()){
                        $this->jsonData["Bandera"] = 1;
                        $this->jsonData["mensaje"] = "El usuario se a creado satisfactoriamente";
                    }else{
                        $this->jsonData["Bandera"] = 0;
                        $this->jsonData["mensaje"] = "Error: al activar la cuenta del usuario";
                    }
                }else{
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] = "Error: al ingresar el nuevo usuario";
                }
                break;
            case 'edit':
                if($this->setUsuarios()){
                    $this->jsonData["Bandera"] = 1;
                    $this->jsonData["mensaje"] = "El usuario se a creado satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] = "Error: al activar la cuenta del usuario";
                }
                break;
            case 'delete':
                if($this->setUsuarios()){
                    $this->formulario->Usuarios->_idSeguridad = $this->getIdSeguridad();
                    if($this->setSeguridad()){
                        $this->jsonData["Bandera"] = 1;
                        $this->jsonData["mensaje"] = "La cuenta del usuario se ha dado de baja";
                    }else{
                        $this->jsonData["Bandera"] = 0;
                        $this->jsonData["mensaje"] = "Error: al dar de baja la cuenta del usuario";
                    }
                }else{
                    $this->jsonData["Bandera"] = 0;
                    $this->jsonData["mensaje"] = "Error: al dar de baja la cuenta del usuario";
                }
                break;
            case 'get':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getUsuarios();
                break;
            case 'getOne':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getUsuario();
                break;
            case 'sucursales':
            case 'institucion':
                $this->jsonData["Bandera"] = 1;
                $this->jsonData["Data"] = $this->getInstitutos();
                break;
        }
        
        print json_encode($this->jsonData);
    }
    
    private function setUsuarios (){
        $fecha = date("Y-m-d");
        switch ($this->formulario->Usuarios->opc){
            case 'new':
                $_idCampus = $_SESSION["rol"]=="root"? 0 : $this->formulario->Usuarios->_idCampus;
                $_idInstitucion = $_SESSION["rol"] == "root"? $this->formulario->Usuarios->_idInstitucion: $_SESSION["_idInstitucion"];
              $sql = "INSERT INTO Usuarios( Nombre, Ap_Paterno, Ap_Materno, email, Username, USRCreacion, FechaCreacion, USRModifico, FechaModifico, _idCampus, _idInstitucion, Estatus) values"
                    . "('{$this->formulario->Usuarios->Nombre}','{$this->formulario->Usuarios->Ap_Paterno}','{$this->formulario->Usuarios->Ap_Materno}','{$this->formulario->Usuarios->email}',"
                    . "'{$this->formulario->Usuarios->email}','{$_SESSION["usr"]}','$fecha','{$_SESSION["usr"]}','$fecha', $_idCampus, $_idInstitucion,1)";
                break;
            case 'edit':
                $sql = "UPDATE Usuarios SET Nombre = '{$this->formulario->Usuarios->Nombre}', Ap_Paterno = '{$this->formulario->Usuarios->Ap_Paterno}', Ap_Materno = '{$this->formulario->Usuarios->Ap_Materno}', "
                . " USRModifico = '{$_SESSION["usr"]}', FechaModifico = '$fecha' where _id = {$this->formulario->Usuarios->_id}  ";
                break;
            case 'delete':
                $sql = "UPDATE Usuarios SET Estatus = 0,  USRModifico = '{$_SESSION["usr"]}', FechaModifico = '$fecha' where _id = {$this->formulario->Usuarios->_id}";
                break;
        }
        
            return $this->conn->query($sql)? true:false;
    }
    
    private function setSeguridad(){
        $fecha = date("Y-m-d");
        switch ($this->formulario->Usuarios->opc){
            case 'new':
                $sql = "INSERT INTO Seguridad(Username, Password, Tipo_usuario, Estatus, USRCreacion, FechaCreacion, USRModifico, FechaModifico, _idUsuarios) "
                . "values ('{$this->formulario->Usuarios->email}',SHA('{$this->jsonData["Password"]}'),'{$this->formulario->Usuarios->rol}',1,'{$_SESSION["usr"]}','$fecha','{$_SESSION["usr"]}','$fecha',"
                        . "'{$this->formulario->Usuarios->last_id}')";
                break;
            
            case 'delete':
                $sql = "UPDATE Seguridad SET Estatus = 0 where _id = ".$this->formulario->Usuarios->_idSeguridad["_id"];
                break;
            
        }
        
        return $this->conn->query($sql);
    }


    private function getPassword(){
        $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            //Obtenemos la longitud de la cadena de caracteres
            $longitudCadena=strlen($cadena);

            //Se define la variable que va a contener la contraseña
            $pass = "";
            //Se define la longitud de la contraseña, en mi caso 10, pero puedes poner la longitud que quieras
            $longitudPass=10;

            //Creamos la contraseña
            for($i=1 ; $i<=$longitudPass ; $i++){
                //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
                $pos=rand(0,$longitudCadena-1);

                //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
                $pass .= substr($cadena,$pos,1);
            }
            return $pass;
    }
    
    private function getUsuarios (){
        $_idCampus = $_SESSION["rol"]=="root"? 0 : $this->formulario->Usuarios->_idInstitucion;
        $_idInstitucion = $_SESSION["rol"] == "root"? $this->formulario->Usuarios->_idInstitucion: $_SESSION["_idInstitucion"];
        $array = array();
        if($_SESSION["usr"]=="root"){
            $sql = "SELECT U._id, concat(U.Ap_Paterno,' ',U.Ap_Materno,' ',U.Nombre) as Nombre, S.Tipo_usuario, U.Username, U.Estatus  FROM Usuarios as U "
                    . "inner join Seguridad as S on (U._id = S._idUsuarios) "
                    . "where U._idInstitucion=$_idInstitucion and U.Estatus = 1";
        }else{
            $sql = "SELECT U._id, concat(U.Ap_Paterno,' ',U.Ap_Materno,' ',U.Nombre) as Nombre, S.Tipo_usuario, U.Username, U.Estatus  FROM Usuarios as U "
                    . "inner join Seguridad as S on (U._id = S._idUsuarios) "
                    . "where U._idInstitucion=$_idInstitucion and U._idCampus = $_idCampus and U.Estatus = 1";
        }
        
        $id = $this->conn->query($sql);
        while ($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    
    private function getUsuario() {
        $sql = "SELECT U._id, U.Nombre, U.Ap_Paterno, U.Ap_Materno, U.email, S.Tipo_usuario as rol from Usuarios as U "
                . "inner join Seguridad as S on (U._id = S._idUsuarios)"
                . " where U._id=".$this->formulario->Usuarios->_idUsr;
        return $this->conn->fetch($this->conn->query($sql));
    }
    private function getIdSeguridad(){
        $sql = "Select _id from Seguridad where _idUsuarios=".$this->formulario->Usuarios->_id;
        return $this->conn->fetch($this->conn->query($sql));
    }
    
    private function getInstitutos(){
        $array = array();
        $status = 1;
        switch($this->formulario->Usuarios->opc){
            case 'institucion':
                $sql = "select _id, Institucion from Instituciones where Estatus = $status";
                break;
            case 'sucursales':
                $sql = "Select _id, Nombre as Institucion  from Campus where _idInstitucion = {$_SESSION["_idInstitucion"]}";
                break;
        }
        
        
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
}

$app = new Usuarios($array_principal);
$app->main();