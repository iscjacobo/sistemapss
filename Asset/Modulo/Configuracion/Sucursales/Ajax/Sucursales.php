<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Instituciones
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');


class Sucursales {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch ($this->formulario->Campus->opc){
                case 'new':
                case 'edit':
                case 'delete':
                case 'Estatus':
                        if($this->setCampus()){
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["mensaje"]=$this->formulario ->Campus->opc == 'new'? 
                                    "La Institucion se registro satisfactoriamente":"La Institucion se edito satisfactoriamente";
                        }else{
                            $this->jsonData["Bandera"]=0;
                            $this->jsonData["mensaje"]= $this->formulario ->Campus->opc == 'new'? 
                                    "Error al registrar la Institucion":"Error al editar la Institucion";
                        }
                break;

                case 'get':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCampus();
                break;

                case 'institucion':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getInstitutos();
                break;

                case 'getEditar':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getEditarCampus();
                break;

                case 'getEntidadFederativa':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getEntidadFederativa();

                
                 break;
                case 'SetFolio':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->NewfoliosCertificados();
                break;
                case 'getFolio':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Folio"] = $this->getfoliosCertificados();
                break;
        }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setCampus(){
       $fecha = date("Y-m-d");
       switch ($this->formulario ->Campus->opc){
            case 'new':
           /*     $sql ="INSERT INTO Campus( Nombre, Domicilio, Colonia, CP, Ciudad, _idEstado, _idInstitucion, Registro_DGP,
                                         USRCreacion, FechaCreacion, USRModifico, FechaModifico, Estatus)
                          values (
                            UPPER( '{$this->formulario->Campus->Nombre}'),
                            UPPER( '{$this->formulario->Campus->Domicilio}'),
                            UPPER( '{$this->formulario->Campus->Colonia}'),
                            UPPER( '{$this->formulario->Campus->CP}'),
                            UPPER( '{$this->formulario->Campus->Ciudad}'),
                            UPPER( '{$this->formulario->Campus->Estado}'),
                            UPPER( '{$this->formulario->Instituto->id}'),   
                            UPPER( '{$this->formulario->Campus->Registro_DGP}'), 
                                '{$_SESSION["usr"]}',
                                '$fecha',
                                '{$_SESSION["usr"]}',
                                '$fecha',
                                '1'
                                 ) 
                             ";*/
                break;
            case 'edit':
                  $sql = "UPDATE Campus SET "
                        ."Nombre =  UPPER('{$this->formulario->Campus->Nombre}'),"
                        ."Domicilio =  UPPER('{$this->formulario->Campus->Domicilio}'),"
                        ."Colonia =  UPPER('{$this->formulario->Campus->Colonia}'),"
                        ."CP =  UPPER('{$this->formulario->Campus->CP}'),"
                        ."Ciudad =  UPPER('{$this->formulario->Campus->Ciudad}'),"   
                        ."_idEstado =  UPPER('{$this->formulario->Campus->_idEstado}'),"
                        ."Registro_DGP =  UPPER('{$this->formulario->Campus->Registro_DGP}'),"
                        ."USRCreacion = '{$_SESSION["usr"]}',"
                        ."FechaCreacion = '$fecha',"  
                        ."USRModifico = '{$_SESSION["usr"]}',"
                        ."FechaModifico= '$fecha'"
                       . " where _id = '{$this->formulario->idSucursal}'";

                       $this->updateFolio();
                break;
              break;
              case 'Estatus':

                  $status = $this->formulario->historico == "true"? 1:0;
                      $sql = "UPDATE Campus SET "
                        ."Estatus = '{$status}',"
                        ."USRModifico = '{$_SESSION["usr"]}',"
                        ."FechaModifico= '$fecha'"
                        . " where _id = '{$this->formulario->idSucursal}'";

              break;
             
          
        }
        return $this->conn->query($sql);
    }
    
    private function getInstitutos(){
        $array = array();
        $status = 1;
        $sql = "select _id, Institucion from Instituciones where Estatus = $status";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getCampus(){
        $array = array();
        $status = $this->formulario->historico  == "true"? 0:1;
        $Instituto = $this->formulario->Instituto->id;
        $sql = "select _id, Nombre, Estatus from Campus where _idInstitucion = $Instituto and Estatus = $status";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getEntidadFederativa(){
        $array = array();
        $sql = "SELECT * FROM EntidadesFederativas";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }


    private function getEditarCampus(){
        $idSucursal = $this->formulario->idSucursal;
        $sql = "select * from Campus where _id = $idSucursal";
     
      
        return $this->conn->fetch($this->conn->query($sql));
        }

    private function NewfoliosCertificados(){
        $fecha = date("Y-m-d");
            $sql = "SELECT * FROM FoliosCertificados WHERE _idSucursal = '{$this->formulario->idSucursal}'";
            $this->conn->fetch($this->conn->query($sql));
            if($this->conn->count_rows()>0){
                    echo "ya hay un registro";
            }
            else{
                       $sql ="INSERT INTO FoliosCertificados( _idSucursal, folio,USRCreacion,FechaCreacion,USRModifico,FechaModifico)
                        values (
                       '{$this->formulario->idSucursal}',
                       '0',
                       '{$_SESSION["usr"]}',
                       '$fecha',
                       '{$_SESSION["usr"]}',
                       '$fecha'
                        ) 
                        ";
                      $this->conn->query($sql);
            }    
    return 1;

    }

    private function getfoliosCertificados(){
       $sql="SELECT * FROM FoliosCertificados WHERE _idSucursal = {$this->formulario->idSucursal}";
            return $this->conn->fetch($this->conn->query($sql));
    }

    private function updateFolio(){
        $fecha = date("Y-m-d");
        $Folio = $this->formulario->Folio->folio ;
        $sql = "UPDATE FoliosCertificados SET "

        ."folio =  '$Folio',"
        ."_idInstitucion =  '{$this->formulario->Campus->_idInstitucion}',"
        ."USRModifico = '{$_SESSION["usr"]}',"
        ."FechaModifico= '$fecha'"
       . " where _id = {$this->formulario->Folio->_id}";
       $this->conn->query($sql);
    
    }
};

$app = new Sucursales($array_principal);
$app->principal();