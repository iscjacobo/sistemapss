
var url = "./Modulo/Configuracion/Sucursales/Ajax/Sucursales.php";
tituloElectronico
                 .controller('SucursalesCtrl',['$scope','$http',SucursalesCtrl])
                 .controller('SucursalesEditCtrl',['$scope','$http',SucursalesEditCtrl]);
function SucursalesCtrl($scope,$http){
    var obj = $scope;
    obj.arrayInstituciones =[];
    obj.arrayCampus =[];
    obj.Institucion={};
    
    obj.formEnabled = false;
    obj.Sucursal = { 
        Nombre:"",
        Domicilio:"",
        Colonia:"",
        CP:"",
        Ciudad:"",
        Estado:"",
        Registro_DGP:"",
        opc:"" 
    };
    obj.historico = false;
    obj.btnShow = false; 
    obj.rol = "";
    console.log(obj.rol);

    obj.btnNuevo=()=>{
        obj.Sucursal = { 
            Nombre:"",
            Domicilio:"",
            Colonia:"",
            CP:"",
            Ciudad:"",
            Estado:"",
            Registro_DGP:"",
            opc:"new" 
        };
        obj.formEnabled = false;
    }


    obj.Entidad_federativa;

    //Es para traer las sucursales

    obj.Entidad_federativa = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"getEntidadFederativa"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Entidad_federativa = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getCampus= ()=>{

            $http({
                method: 'POST',
                    url: url,
                    data: {Campus: {opc:"get"}, Instituto: obj.Institucion, historico : obj.historico}
                }).then(function successCallback(res){
                    if(res.data.Bandera == 1){
                        obj.arrayCampus = res.data.Data;
                        obj.btnShow = true;

                    }else{
                       console.log(res.data.mensaje);
                    }
                }, function errorCallback(res){
            });
    };
    //Es para traer las Instituciones
    obj.getInstituciones = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"institucion"}}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayInstituciones = res.data.Data;

                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    };
    //Para guardar una sucursal
    obj.btnGuardarSucursal = ()=>{
        obj.Sucursal.opc="new";
        console.log(obj.Sucursal);
        $http({
            method: 'POST',
            url: url,
            data: {Campus: obj.Sucursal, Instituto: obj.Institucion}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {           
                toastr.success(res.data.mensaje);  
                obj.formEnabled = true;         

            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
     
    }
    //Para madar el ID de la sucursal a html editar
    obj.btnEditar = (e) =>{
        window.location.href = "?mod=Sucursales&opc=editar&id="+e;
    }
    //Dar de baja una sucursal
    obj.btnBaja = (e)=>{
        if(confirm("Estas seguro de dar de baja el campus")){
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"Estatus"}, historico : obj.historico , idSucursal: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCampus();
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    }
    //dar de alta una sucursal
    obj.btnAlta = (e)=>{
        if(confirm("¿Estas seguro de dar de alta el campus?")){
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"Estatus"}, historico : obj.historico , idSucursal: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCampus();
                }else{
                console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
      }
    }
    obj.btnRegresar = ()=>{
        location.href="?mod=Sucursales";
    }
    
    //lo que tiene que ejucutar cuando arranca
    angular.element(document).ready(function(){
        obj.Entidad_federativa();
        console.log(obj.Institucion);
        switch(obj.rol){
            case 'root':
                obj.getInstituciones();
                obj.Entidad_federativa();
                
                break;
            case 'Admin':
                obj.getCampus();
                obj.Entidad_federativa();
                break;
        }
            
     });
};

function SucursalesEditCtrl($scope,$http){
    var obj = $scope;
    obj.idSucursal={};
    obj.Folio={};
    obj.formEnabled = false;
    obj.Sucursal = { 
        opc:"" 
    };
    obj.Entidad_federativa;

    obj.Entidad_federativa = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"getEntidadFederativa"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Entidad_federativa = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
 
    obj.btnEditar = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Sucursal: obj.Sucursal, idSucursal:  obj.idSucursal.id, Campus: {opc :"getEditar"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.formEnabled = true;     
                    obj.Sucursal = res.data.Data;               
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
                
        });
    }
        
    obj.btnEditarSucursal = ()=>{
        obj.Sucursal.opc="edit";
        $http({
            method: 'POST',
            url: url,
            data: {Campus: obj.Sucursal, idSucursal : obj.idSucursal.id, Folio: obj.Folio}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {           
                toastr.success(res.data.mensaje);  
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }

    obj.btnRegresar = ()=>{
        location.href="?mod=Sucursales";
    }
    obj.newFolioCertificado = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"SetFolio"}, Instituto: obj.Institucion, idSucursal:  obj.idSucursal.id}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
             
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getIdFolio=()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Campus: {opc:"getFolio"} ,idSucursal:  obj.idSucursal.id}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Folio = res.data.Folio
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    
      angular.element(document).ready(function(){
       obj.btnEditar();
       obj.Entidad_federativa();
       obj.newFolioCertificado();
       obj.getIdFolio();
       console.log(  obj.Sucursal);

     });
};