/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var url = "./Modulo/Configuracion/Instituciones/Ajax/Instituciones.php";
tituloElectronico
                .controller('InstitucionesCtrl',['$scope','$http',InstitucionesCtrl])
                .controller('InstitucioneseditCtrl',['$scope','$http',InstitucioneseditCtrl]);

function InstitucionesCtrl($scope,$http){
    var obj = $scope;
    obj.arrayInstituciones =[];
    obj.Instituto = {
        opc:"" 
    };
    obj.historico = false; 
    obj.arrayEntidad = [];
    obj.arrayCargo = [];
    obj.btnNuevo=()=>{
        obj.Instituto = { 
            id:"",
            Insitucion:"",
            _idEntidadFederativa:"",
            Nombre:"",
            Ap_Paterno:"",
            Ap_Materno:"",
            Curp:"",
            _idCargo:"",
            key:"",
            cer:"",
            opc:"new" 
        };
        obj.formEnabled = false;
    }
    obj.catalogos = () =>{
        $http({
            method: 'POST',
            url: url,
            data: {Instituto: {opc: "Catalogos"}}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                obj.arrayEntidad = res.data.entidad;
                obj.arrayCargo = res.data.cargo;
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    obj.btnNuevoInstituto = ()=>{
        obj.Instituto.Instituto = "";
        obj.Instituto.opc = "new";
        $("#ModalInstituto").modal("show");
    };
    obj.getInstitutos = ()=>{
        obj.Instituto.opc = "get";
        obj.Instituto.historico = obj.historico;
        $http({
            method: 'POST',
            url: url,
            data: {Instituto: obj.Instituto}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                obj.arrayInstituciones = res.data.Data;
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    };
    obj.btnGuardarInstituto = ()=>{
        obj.Instituto.opc = "new"
        $http({
            method: 'POST',
            url: url,
            data: {Instituto: obj.Instituto}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                toastr.success(res.data.mensaje);
                obj.formEnabled = true;
                 
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    };
    obj.btnRegresar = ()=>{
        location.href="?mod=Instituciones";
    }
    obj.btnEditar = (e)=>{
        location.href="?mod=Instituciones&opc=editar&id="+e;
    };
    obj.btnBaja = (e)=>{
        if(confirm("Estas seguro de dar de baja el Instituto")){
            obj.Instituto = e;
            obj.Instituto.opc = "delete";
            $http({
                method: 'POST',
                url: url,
                data: {Instituto: obj.Instituto}
            }).then(function successCallback(res) {
                console.log(res.data);
                if (res.data.Bandera == 1) {
                    $("#ModalInstituto").modal("hide");   
                    toastr.success(res.data.mensaje);
                     obj.getInstitutos();

                } else {
                    toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res) {
            });
        }
        
    };
    obj.btnAlta = (e)=>{
        if(confirm("¿Estás seguro de dar de alta el Instituto?")){
            obj.Instituto = e;
            obj.Instituto.opc = "Alta";
            $http({
                method: 'POST',
                url: url,
                data: {Instituto: obj.Instituto}
            }).then(function successCallback(res) {
                console.log(res.data);
                if (res.data.Bandera == 1) {
                    $("#ModalInstituto").modal("hide");   
                    toastr.success(res.data.mensaje);
                     obj.getInstitutos();

                } else {
                    toastr.error(res.data.mensaje);
                }
            }, function errorCallback(res) {
            });
        }
        
    };
    obj.btnEditarInstituto = ()=>{
        $http({
            method: 'POST',
            url: url,
            data: {Instituto: obj.Instituto}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                $("#ModalInstituto").modal("hide");   
                toastr.success(res.data.mensaje);
                 obj.getInstitutos();
                 
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    };
    angular.element(document).ready(function(){
         obj.getInstitutos();
         obj.catalogos();
              

     });
}

function InstitucioneseditCtrl($scope,$http){
    var obj = $scope;
    obj.Institucion = {};
    obj.arrayEntidad = [];
    obj.arrayCargo = [];
    obj.Instituto = {};
    obj.btnRegresar = ()=>{
        location.href="?mod=Instituciones";
    }

    obj.catalogos = () =>{
        $http({
            method: 'POST',
            url: url,
            data: {Instituto: {opc: "Catalogos"}}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                obj.arrayEntidad = res.data.entidad;
                obj.arrayCargo = res.data.cargo;
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }


    obj.getInstituto = () =>{
        $http({
            method: 'POST',
            url: url,
            data: {Instituto:{opc: "getInstituto"}, _idInstituto : obj.Institucion.id}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                obj.Instituto = res.data.Instituto;

            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    obj.btnUpdateInstitucion = () =>{
        $http({
            method: 'POST',
            url: url,
            data: {Instituto:{opc: "edit"}, Institucion : obj.Instituto}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {
                toastr.success(res.data.mensaje);
                obj.getInstituto();


            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }


    angular.element(document).ready(function(){
        obj.catalogos();
        obj.getInstituto();

    });

}