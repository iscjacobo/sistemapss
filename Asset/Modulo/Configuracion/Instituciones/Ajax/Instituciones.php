<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Instituciones
 *
 * @author Manuel Jacobo
 */
error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');


class Instituto {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        
        
        switch ($this->formulario ->Instituto->opc){
            case 'new':
            case 'edit':
            case 'delete':
            case 'Alta':
                if($this->setInstitutos()){
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["mensaje"]=$this->formulario ->Instituto->opc == 'new'? 
                            "La Institucion se registro satisfactoriamente":"La Institucion se edito satisfactoriamente";
                }else{
                    $this->jsonData["Bandera"]=0;
                    $this->jsonData["mensaje"]= $this->formulario ->Instituto->opc == 'new'? 
                            "Error al registrar la Institucion":"Error al editar la Institucion";
                }
                break;
            case 'get':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Data"] = $this->getInstitutos();
                break;
            case 'Catalogos':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["entidad"] = $this->getEntidad();
                    $this->jsonData["cargo"] = $this->getCargo();
            break;
            case 'getInstituto':
                    $this->jsonData["Bandera"]=1;
                    $this->jsonData["Instituto"] = $this->getInstituto();
              
            break;
                       
            }
        
        print json_encode($this->jsonData);
        
    }
    
    private function setInstitutos(){
      
       $fecha = date("Y-m-d");
       switch ($this->formulario ->Instituto->opc){
            case 'new':
             $sql = "INSERT INTO Instituciones( _idInstitucion, Institucion, FechaCreacion,USRCreacion,FechaModificacion, USRModificacion,
             Nombre, Ap_Paterno,Ap_Materno,CURP,_idCargo,NoCertificadoResponsable,CertificadoResponsable,Estatus) "
                . "values (
                    '{$this->formulario ->Instituto->id}',
                     UPPER('{$this->formulario ->Instituto->Institucion}'),
                     '$fecha', '{$_SESSION["usr"]}',
                     '$fecha', '{$_SESSION["usr"]}',
                     UPPER('{$this->formulario ->Instituto->Nombre}'),
                     UPPER('{$this->formulario ->Instituto->Ap_Paterno}'),
                     UPPER('{$this->formulario ->Instituto->Ap_Materno}'),
                     UPPER('{$this->formulario ->Instituto->CURP}'),
                     '{$this->formulario ->Instituto->_idCargo}',
                     '{$this->formulario ->Instituto->NoCertificadoResponsable}',
                     '{$this->formulario ->Instituto->CertificadoResponsable}',
                      '1')";
                break;
            case 'edit':
              $sql = "UPDATE Instituciones SET "
                        . "_idInstitucion = {$this->formulario->Institucion->_idInstitucion},"
                        . "Institucion = UPPER('{$this->formulario->Institucion->Institucion}'),"
                        . "Nombre = UPPER('{$this->formulario ->Institucion->Nombre}'),"
                        . "Ap_Paterno = UPPER('{$this->formulario ->Institucion->Ap_Paterno}'),"
                        . "Ap_Materno = UPPER('{$this->formulario ->Institucion->Ap_Materno}'),"
                        . "CURP = UPPER('{$this->formulario ->Institucion->CURP}'),"
                        . "_idCargo = UPPER('{$this->formulario ->Institucion->_idCargo}'),"
                        . "NoCertificadoResponsable = ('{$this->formulario ->Institucion->NoCertificadoResponsable}'),"
                        . "CertificadoResponsable = ('{$this->formulario ->Institucion->CertificadoResponsable}'),"
                        . "FechaModificacion = '$fecha',"
                        . "USRModificacion = '{$_SESSION["usr"]}' where _id = {$this->formulario->Institucion->_id}
                ";
              break;
              case 'delete':
              $sql = "UPDATE Instituciones SET "
              . "Estatus = 0 ,"
              . "FechaModificacion = '$fecha',"
              . "USRModificacion = '{$_SESSION["usr"]}' where _id = {$this->formulario->Instituto->_id}
              ";
              break;
              case 'Alta':
              $sql = "UPDATE Instituciones SET "
              . "Estatus = 1 ,"
              . "FechaModificacion = '$fecha',"
              . "USRModificacion = '{$_SESSION["usr"]}' where _id = {$this->formulario->Instituto->_id}
              ";

              break;
          
        }
        return $this->conn->query($sql);
    }
    
    private function getInstitutos(){
        $array = array();
        $status = $this->formulario->Instituto->historico == "true"? 0:1;
        $sql = "select * from Instituciones where Estatus = $status";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getEntidad(){
        $array = array();
        $sql = "select * from EntidadesFederativas ";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getCargo(){
        $array = array();
        $sql = "select * from Cargo_firmantes_autorizados ";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    private function getInstituto(){
        $sql ="SELECT * FROM Instituciones WHERE _id = '{$this->formulario->_idInstituto}' ";

        return $this->conn->fetch($this->conn->query($sql));
    }
}

$app = new Instituto($array_principal);
$app->principal();
