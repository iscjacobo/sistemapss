<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //$dasxml = SDO_DAS_XML::create("../../XML/Plantilla/Certificado.xsd");
$objetoXML = new XMLWriter();

// Estructura básica del XML
$objetoXML->openURI("XML/Certificados/certificado_07020154.xml");
$objetoXML->setIndent(true);
$objetoXML->setIndentString("\t");
$objetoXML->startDocument('1.0', 'utf-8','yes');
$objetoXML->startElement("Dec");
$objetoXML->writeAttribute("version", "2.0");
$objetoXML->writeAttribute("tipoCertificado", "5");
$objetoXML->writeAttribute("folioControl", "1");
$objetoXML->writeAttribute("sello", "revisar manual");
$objetoXML->writeAttribute("certificadoResponsable", "revisar manual");
$objetoXML->writeAttribute("noCertificadoResponsable", "revisar manual");
$objetoXML->writeAttribute("xmlns", "https://www.siged.sep.gob.mx/certificados");
    $objetoXML->startElement("Ipes");
    $objetoXML->writeAttribute("idNombreInstitucion", "20157");
    $objetoXML->writeAttribute("nombreInstitucion", "Nombre de la institucion"); //Atributo opcional
    $objetoXML->writeAttribute("idCampus", "20157");
    $objetoXML->writeAttribute("idEntidadFederativa", "20157");
        $objetoXML->startElement("Responsable");
            $objetoXML->writeAttribute("curp", "RAAF833007HMN"); //ATRIBUTO DEL CURP DEL RESPONSABLE DE FIRMAR
            $objetoXML->writeAttribute("nombre", "Francisco ivan"); //ATRIBUTO DEL NOMBRE DEL RESPONSABLE DE FIRMAR
            $objetoXML->writeAttribute("primerApellido", "Ramirez"); //ATRIBUTO DEL APELLIDO PATERNO DEL RESPONSABLE DE FIRMAR
            $objetoXML->writeAttribute("segundoApellido", "Alcaraz"); //ATRIBUTO DEL APELLIDO MATERNO DEL RESPONSABLE DE FIRMAR
            $objetoXML->writeAttribute("idCargo", "3574"); //ATRIBUTO DEL CURP DEL RESPONSABLE DE FIRMAR
        $objetoXML->endElement();
    $objetoXML->endElement();
    $objetoXML->startElement("Rvoe");
        $objetoXML->writeAttribute("numero", "2014578"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->writeAttribute("fechaExpedicion", "2019-10-11T00:00:00"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
    $objetoXML->endElement();
    $objetoXML->startElement("Carrera");
        $objetoXML->writeAttribute("idCarrera", "2014578"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->writeAttribute("idTipoPeriodo", "121545"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("clavePlan", "121545"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("idNivelEstudios", "12124"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("calificacionMinima", "5"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("calificacionMaxima", "10"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("calificacionMinimaAprobatoria", "7"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
    $objetoXML->endElement();
    $objetoXML->startElement("Alumno");
        $objetoXML->writeAttribute("numeroControl", "2014578"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->writeAttribute("curp", "121545"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("nombre", "ISABEL"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("primerApellido", "FUENTES"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("segundoApellido", "SALOMON"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("idGenero", "70"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("fechaNacimiento", "AAAA-MM-DDT00:00:00"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
    $objetoXML->endElement();
    $objetoXML->startElement("Expedicion");
        $objetoXML->writeAttribute("idTipoCertificacion", "2014578"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->writeAttribute("fecha", "121545"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("idLugarExpedicion", "ISABEL"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
    $objetoXML->endElement();
    $objetoXML->startElement("Asignaturas");
        $objetoXML->writeAttribute("total", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->writeAttribute("asignadas", "16"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("promedio", "9.45"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("totalCreditos", "20"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->writeAttribute("creditosObtenidos", "20"); //ATRIBUTO DEL FECHA DE EXPEDICION DEL REVOE
        $objetoXML->startElement("Asignatura");
            $objetoXML->writeAttribute("idAsignatura", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("ciclo", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("calificacion", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("idObservaciones", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("idTipoAsignatura", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("creditos", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->endElement();
        $objetoXML->startElement("Asignatura");
            $objetoXML->writeAttribute("idAsignatura", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("ciclo", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("calificacion", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("idObservaciones", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("idTipoAsignatura", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
            $objetoXML->writeAttribute("creditos", "16"); //ATRIBUTO DEL NUMERO DEL REVOE
        $objetoXML->endElement();
    $objetoXML->endElement();
    
$objetoXML->endElement(); // Final del nodo raíz, "Dec"
$objetoXML->endDocument(); // Final del documento