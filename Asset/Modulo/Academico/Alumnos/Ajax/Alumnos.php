<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 * Description of Carreras
 *
 * @author Manuel Jacobo
 */


error_reporting(E_ALL);
ini_set('display_errors', '0');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');


class Alumnos {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }   
    public function __destruct() {
        unset($this->conn);
    }
    public function principal(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch ($this->formulario->Alumnos->opc){
                case 'new':
                case 'edit':
                case 'delete':
                case 'Estatus':
                        if($this->setAlumnos()){
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["mensaje"]=$this->formulario->Alumnos->opc == 'new'? 
                                                       "El alumno se registro satisfactoriamente":"El alumno se edito satisfactoriamente";
                                                       if($this->formulario->Alumnos->opc == 'edit'){
                                                        $this->editCalificaciones();
                                                        $this->UpdateAntecedentes();
                                                    }
                        }else{
                            $this->jsonData["Bandera"]=0;
                            $this->jsonData["mensaje"]= $this->formulario ->Alumnos->opc == 'new'? 
                                                       "Error al registrar el alumno":"Error al editar el alumno";
                        }
                break;
                case 'getInstituciones':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getInstituciones();
                            $this->jsonData["mensaje"]="Listo";            
                 break;
                case 'getAlumnos':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAlumnos();
                            $this->jsonData["mensaje"]="Listo";
                break;
                case 'getCarreras':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCarreras();
                            $this->jsonData["mensaje"]="Listo";
                break;
                case 'getCampus':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCampus();
                            $this->jsonData["mensaje"]="Listo";            
                break;
                 case 'getAlumno':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAlumno();
                            $this->jsonData["mensaje"]="Listo";
                break;
                case 'getCarrera':
                            $this->jsonData["Bandera"]=      1;
                            $this->jsonData["Carrera"] =     $this->getCarrera();
                            $this->jsonData["Asignaturas"] = $this->getAsignaturas();                 
                            $this->jsonData["mensaje"]=      "Listo";
                break;
                case 'getAsignaturas':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAsignaturas();
                            $this->jsonData["mensaje"]="Listo";
                break;   
                case 'SetCalificaciones':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->setCalificaciones();
                            $this->jsonData["mensaje"]="Listo";
                 break;   
                 case 'getCalificaciones':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Carrera"] =     $this->getCarrera();
                            $this->jsonData["Calificacion"] = $this->getCalificaciones();
                            $this->jsonData["mensaje"]="Listo";
                 break;   
                 case 'setAntecedentes':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->setAntecedentes();
                 break;
                 case 'getAntecedentes':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getAntecedentes();
                break;
                case 'getT':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Observaciones"] = $this->getTipo_Observaciones();
                            $this->jsonData["Entidad"] = $this->getEntidad();
                            $this->jsonData["Sexo"] = $this->getSexo();
                            $this->jsonData["Estudio"] = $this->getEstudio();
                  break;
                case 'Sexo':
                        $this->jsonData["Bandera"]=1;
                        $this->jsonData["Data"] = $this->getSexo();
                    break;
            
        }        
        print json_encode($this->jsonData);        
    }

    private function getInstituciones(){
       $array = array();
       $sql = "SELECT _id, Institucion FROM Instituciones where Estatus = '1'";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
         array_push($array, $row);
        }  
        return $array;
    }

    private function getCampus(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];
        $_idInstitucion = $_SESSION["_idInstitucion"];
                switch ("$rol") {
                    case 'root':
                    $sql="SELECT _id, Nombre from Campus WHERE _idInstitucion = '{$this->formulario->Instituto->_idInstituto}' and Estatus = 1";
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                        break;

                    case 'Admin';
                        $sql="SELECT _id, Nombre FROM Campus WHERE _idInstitucion = ' $_idInstitucion' AND Estatus = 1 ";    
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                    break;

                                  
                    
            }
        return $array;
    }

    private function getCarreras(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];
        $_iCampus = $_SESSION["_idCampus"];
                switch ("$rol") {
                    case 'root':
                    case 'Admin';
                       $sql = "SELECT  _idCarrera, _id, Carrera, Modulos, _idCampus FROM Carreras WHERE Estatus = '1'
                       AND  _idCampus = '{$this->formulario->Campus->_idCampus}'";
                         $id = $this->conn->query($sql);
                         while($row = $this->conn->fetch($id)){
                          array_push($array, $row);
                         }  
                    break;

                    case 'CtrlEsc':
                        $sql = "SELECT  _idCarrera, _id, Carrera, Modulos, _idCampus FROM Carreras WHERE Estatus = '1' 
                        and _idCampus = '$_iCampus'";  
                           $id = $this->conn->query($sql);
                           while($row = $this->conn->fetch($id)){
                            array_push($array, $row);
                           }  
                    break;                   
                    
            }
        return $array;
    }

    private function getSexo(){
        $array = array();
        $sql = "SELECT  * FROM Catalogo_Genero";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getCarrera(){
        $array = array();
        
        $sql = "SELECT  _id, Carrera, Modulos FROM Carreras where _id = '{$this->formulario->_idCarrera}'  ";
        
        return $this->conn->fetch($this->conn->query($sql));
    }

    private function getAlumnos(){
        $array = array();
        $status = $this->formulario->historico  == "true"? 0:1;
        $sql = "SELECT  * FROM Alumnos where _idCarrera = '{$this->formulario->Carrera->id}' and Estatus = $status";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getAlumno(){
        $array = array();
        $sql = "SELECT  * FROM Alumnos where _id = '{$this->formulario->_idAlumno->id}'  ";
        
        return $this->conn->fetch($this->conn->query($sql));
    }

    private function setAlumnos() {
       
        switch ($this->formulario->Alumnos->opc) {
            case 'new':
                       $sql = "INSERT INTO Alumnos( Curp, USR, Nombre, Ap_Paterno, Ap_Materno, email, _idCampus, _idCarrera,
                                            Estatus,_idGenero, FechaNacimiento )"
                        . "values ("
                        ."UPPER('{$this->formulario->Alumnos->Curp}'), "
                        . "'{$this->formulario->Alumnos->USR}',"
                        . "UPPER('{$this->formulario->Alumnos->Nombre}'),"
                        . "UPPER('{$this->formulario->Alumnos->Ap_Paterno}'),"
                        . "UPPER('{$this->formulario->Alumnos->Ap_Materno}'),"
                        . "'{$this->formulario->Alumnos->email}',"
                        . "'{$_SESSION["_idCampus"]}',"
                        . "'{$this->formulario->Alumnos->_idCarrera}',"
                        . "'1',"
                        . "'{$this->formulario->Alumnos->_idGenero}',"
                        . "'{$this->formulario->Alumnos->FechaNacimiento}'"                   
                        .")";
                      
            break;
            case 'edit';
                        $sql = "UPDATE Alumnos SET 
                        Curp= UPPER('{$this->formulario->Alumnos->Curp}'), 
                        USR= UPPER('{$this->formulario->Alumnos->USR}'),
                        Nombre= UPPER('{$this->formulario->Alumnos->Nombre}'), 
                        Ap_Paterno= UPPER('{$this->formulario->Alumnos->Ap_Paterno}'), 
                        Ap_Materno= UPPER('{$this->formulario->Alumnos->Ap_Materno}'), 
                        email= UPPER('{$this->formulario->Alumnos->email}'), 
                        _idCampus= UPPER('{$this->formulario->Alumnos->_idCampus}'), 
                        _idCarrera= UPPER('{$this->formulario->Alumnos->_idCarrera}'), 
                        _idGenero= UPPER('{$this->formulario->Alumnos->_idGenero}'), 
                        FechaNacimiento= UPPER('{$this->formulario->Alumnos->FechaNacimiento}') 
                         where _id = {$this->formulario->Alumnos->_id}";
                       

        
            break;
            case  'Estatus':

                        $status = $this->formulario->historico == "true"? 1:0;
                        $sql = "UPDATE Alumnos SET "
                        ."Estatus = '{$status}'"
                        . " where _id = '{$this->formulario->_idAlumno}'";
                 


            break;
            
        }
        return $this->conn->query($sql);
    }
    
    private function setAntecedentes(){

             $sql = "SELECT * FROM AntecedentesAlumno WHERE _idAlumno = '{$this->formulario->_idAlumno->id}'";
             $this->conn->fetch($this->conn->query($sql));

                if($this->conn->count_rows()>0){
                        echo "ya hay uno registro";
                }
                else{
                      echo $sql = "INSERT INTO AntecedentesAlumno ( _idAlumno, institucionProcedencia, _idTipoEstudioAntecedente,
                                 _idEntidadFederativa, fechaInicio, fechaTerminacion, NoCedula)"
                                    . "values ("
                                    ."'{$this->formulario->_idAlumno->id}', "
                                    . "' ',"
                                    . "'0',"
                                    . "'0',"
                                    . "'0000-00-00',"
                                    . "'0000-00-00',"
                                    . "'0')";
                                   $this->conn->query($sql);
                }    

                return 1;
        
    }

    private function getAntecedentes(){
        $sql="SELECT * FROM AntecedentesAlumno WHERE _idAlumno = '{$this->formulario->_idAlumno->id}'";
        return $this->conn->fetch($this->conn->query($sql));
    }

    private function UpdateAntecedentes() {  
        $sqlAntecedentes = "UPDATE AntecedentesAlumno SET 
        institucionProcedencia= UPPER('{$this->formulario->Antecedentes->institucionProcedencia}'), 
        _idTipoEstudioAntecedente= UPPER('{$this->formulario->Antecedentes->_idTipoEstudioAntecedente}'),
        _idEntidadFederativa= UPPER('{$this->formulario->Antecedentes->_idEntidadFederativa}'), 
        fechaInicio= UPPER('{$this->formulario->Antecedentes->fechaInicio}'), 
        fechaTerminacion= UPPER('{$this->formulario->Antecedentes->fechaTerminacion}'), 
        NoCedula= UPPER('{$this->formulario->Antecedentes->NoCedula}')
        where _id = {$this->formulario->Antecedentes->_id}";
        $this->conn->query($sqlAntecedentes);
    }
     
    private function getAsignaturas(){
        /*Armar el arreglo de las materias*/
        $a = array();
        $array = array("grados"=>array());
        $c=0;
        for($i = 0; $i < $this->jsonData["Carrera"]["Modulos"]; $i++){ 
            $sql = "SELECT _id, Asignatura, modulo FROM Asignaturas where _id_carrera = {$this->formulario->_idCarrera} and modulo= ". ($i+1) ."; ";
            $id = $this->conn->query($sql);
            if($this->conn->count_rows()!=0){
                $a = array(); //se inicializa o se limpia el array cada vez que entre a este ciclo
                while($row  = $this->conn->fetch($id)){
                    array_push($a, array("materia"=>$row));
                }
                $array["grados"][$i] = $a;
            }else{
                $array["grados"][$i] = array();
            }
            
        }  
        return $array;
        
    }
    
    private function setCalificaciones(){
        $array = array();
        $fecha = date("Y-m-d");
        foreach ($this->formulario->Asignaturas->grados as $key => $value){
            foreach ($value as $materia){
                        $sql = "Select * from Calificaciones where _idAsignatura = '{$materia->materia->_id}' and _idAlumno = '{$this->formulario->Alumnos->Alumno->_id}' ";
                        $this->conn->fetch($this->conn->query($sql));

                       if($this->conn->count_rows()>0){
                            
                       }
                       else{
                            $sql = "INSERT INTO Calificaciones ( _idCarrera, _idAlumno, _idAsignatura, Modulo, Calificacion, USRCreacion, FechaCreacion, USRmodificacion, FechaModificacion )"
                                        . "values ('{$this->formulario->Carrera->_id}','{$this->formulario->Alumnos->Alumno->_id}','{$materia->materia->_id}','{$materia->materia->modulo}',"
                                        . "'0','SYS','{$fecha}','SYS','{$fecha}')";
                                        $this->conn->query($sql);
                       }    

             }
        }
        return     true;
    }

    private function getCalificaciones(){
           /*Armar el arreglo de las materias*/
           $a = array();
           $array = array("grados"=>array());
       
           for($i = 0; $i < $this->jsonData["Carrera"]["Modulos"]; $i++){ 
               $sql = "SELECT _id, _idAsignatura, Modulo, Calificacion, _idObservaciones, Ciclo FROM Calificaciones where _idAlumno = {$this->formulario->Alumno->_id} and _idCarrera = {$this->formulario->Carrera->_id} and modulo= ". ($i+1) ."; ";
               $id = $this->conn->query($sql);
               if($this->conn->count_rows()!=0){
                   $a = array(); //se inicializa o se limpia el array cada vez que entre a este ciclo
                   while($row  = $this->conn->fetch($id)){
                       array_push($a, array("materia"=>$row));
                   }
                   $array["grados"][$i] = $a;
               }else{
                   $array["grados"][$i] = array();
               }
               
           }  
           return $array;
    }

    private function editCalificaciones(){


      $fecha = date("Y-m-d");
      foreach ($this->formulario->Calificaciones->grados as $key => $value){
          foreach ($value as $materia){
            $sql = "UPDATE Calificaciones SET
                    Calificacion='{$materia->materia->Calificacion}',
                    Ciclo='{$materia->materia->Ciclo}',
                    USRModificacion ='{$_SESSION["usr"]}',
                    FechaModificacion = '{$fecha}',
                    _idObservaciones = '{$materia->materia->_idObservaciones}'
                    WHERE _idAlumno = '{$this->formulario->Alumnos->_id}'
                    AND _idAsignatura = '{$materia->materia->_idAsignatura}'";
            $this->conn->query($sql);
           }
      }

    }
   
    private function getTipo_Observaciones(){
        $array = array();
        $sql = "SELECT * FROM Catalogo_Observaciones";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getEntidad(){
        $array = array();
            $sql = "SELECT * FROM EntidadesFederativas";
            $id = $this->conn->query($sql);
            while($row = $this->conn->fetch($id)){
                array_push($array, $row);
            }
        return $array;
    }

    private function getEstudio(){
        $array = array();
        $sql = "SELECT * FROM Estudio_antecedentes WHERE tipoDocumento = 1";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
  
        

        

    
};

$app = new Alumnos($array_principal);
$app->principal();

      