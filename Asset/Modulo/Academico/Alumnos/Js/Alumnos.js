/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var url = "./Modulo/Academico/Alumnos/Ajax/Alumnos.php";
tituloElectronico
                .controller('AlumnosCtrl',['$scope','$http',AlumnosCtrl])
                .controller('EditarAlumnosCtrl',['$scope','$http',EditarAlumnosCtrl])

function AlumnosCtrl($scope,$http){
    var obj = $scope;
    //Variables
    {
    obj.rol="";

    obj.arrayInstituciones =[];
    obj.Instituto ={};

    obj.arrayCampus =[];
    obj.Campus = {};

    obj.arrayCarreras =[];
    obj.Carrera = {};

    obj.arrayAlumnos =[];
    obj.Alumno = {
        USR: "",
        Nombre: "",
        Curp : "",
        Ap_Paterno: "",
        Ap_Materno : "",
        email: "",
        _idGenero: "",
        _idCarrera: "",
        FechaNacimiento: "",
        opc: ""
    };

    obj.arraySexo =[];
    obj.historico = false;
    obj.btnShow = true;  
    obj.formEnabled = false;
    }


    //Limpiar Textbox
    obj.btnNuevo = () =>{
        obj.Alumno = {
            USR: "",
            Nombre: "",
            Curp : "",
            Ap_Paterno: "",
            Ap_Materno : "",
            email: "",
            _idGenero: "",
            _idCarrera: "",
            FechaNacimiento: "",
            opc: ""
        };
        obj.formEnabled = false;
    }
    //Traer las carreras
    obj.getCarreras = ()=>{
        console.log(obj.Campus);
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getCarreras"}, historico : obj.historico, Campus : obj.Campus}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayCarreras = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    };
    //traer alumnos
    obj.getAlumnos = () =>{
      
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getAlumnos"}, historico : obj.historico, Carrera : obj.Carrera}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayAlumnos = res.data.Data;
                 
          
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //funcion para guarda alumno
    obj.btnGuardarAlumno = () =>{
        obj.Alumno.opc="new";
        obj.Alumno.FechaNacimiento = $("#txtfechanac").val()
        console.log(obj.Alumno);
        $http({
            method: 'POST',
            url: url,
            data: {Alumnos: obj.Alumno}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {           
                toastr.success(res.data.mensaje);  
              
                obj.formEnabled  = true; 
               
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    // funcion regresar
    obj.btnRegresar = ()=>{
        location.href="?mod=Alumnos";
    }
    // traer los id de catalogo sexo
    obj.getSexo = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"Sexo"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.arraySexo = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Editamos al alumno
    obj.btnEditar = (e) =>{
        window.location.href = "?mod=Alumnos&opc=editar&id="+e;
    }
    //Damos de baja al alumno
    obj.btnBaja = (e) =>{
        if(confirm("Estas seguro de dar de baja al alumno")){
        
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"Estatus"}, historico : obj.historico , _idAlumno: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getAlumnos();

                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
        }
    }
    //Damos de regreso al alumno    
    obj.btnAlta = (e)=>{
        if(confirm("¿Estas seguro de dar de alta al alumno?")){
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"Estatus"}, historico : obj.historico , _idAlumno: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getAlumnos();
                }else{
                console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
      }
    }

    obj.getCampus = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getCampus"}, historico : obj.historico, Instituto : obj.Instituto}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayCampus = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }

    obj.getInstituciones = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getInstituciones"}, historico : obj.historico}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayInstituciones = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    
    obj.btnGenerarXML = (_id)=>{
        location.href = "?mod=Ecertificado&opc=nuevo&idUSR="+_id;
    }
    //Funcion que se inizaliza sola
    angular.element(document).ready(function(){
        $(".calendario").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            maxDate: '+1w',
                            format: "yyyy-mm-dd",
                            weekStart:1,
                            calendarWeeks: true,
                            language:"es",
                            autoclose: true
            });
        obj.getSexo();
        console.log(obj.rol);
        switch(obj.rol){
            case 'root':
                    obj.getInstituciones();
                break;
            case 'Admin':
                    obj.getCampus();
                break;
            case 'CtrlEsc':
                    obj.getCarreras();
                break;
           
        }
     });
}

function EditarAlumnosCtrl($scope,$http){
    var obj = $scope;
    {
    obj.Alumno= {};
    obj.Antecedente={};
    obj._idAlumno = {};
    obj.Carrera = {
        _id : "",
        Carrera: "",
        Modulos:""
    };
    obj.Asignaturas = {
        Asignatura: "2",
        _id : "",
        modulo : "",
        Cal : 0
    };
    obj.arrayAsiganturas = [];
    obj.Calificaciones = {
            _id : "",
            _idAsignatura : "",
            Modulo : "",
            Calificacion : ""
    }
    obj.arrayCalificaciones = [];
    obj.Tipo_Observaciones;
     obj. arrayEntidad = [];

     obj. arrayTipoEstudio = [];
    }
    //TRAEMOS LOS TIPOS DE OBSERVACIONES
    obj.getTipo_Observaciones= ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getT"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Tipo_Observaciones = res.data.Observaciones;
                    obj.arrayEntidad = res.data.Entidad;
                    obj.arraySexo = res.data.Sexo;
                    obj.arrayTipoEstudio = res.data.Estudio;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    // DESBLOQUEAMOS LAS CAJAS TEXTO
    obj.DesbloquearText = ()=>{
        if(confirm("¿Quieres agregar calificación?")){
        obj.formEnabled = false;
        } 
    }
    //BUSCAMOS AL ALUMNO A EDITAR
    obj.getAlumno = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getAlumno"}, _idAlumno : obj._idAlumno}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Alumno = res.data.Data;
                    obj.btnShow = true; 
                    obj.getCarrera(obj.Alumno._idCarrera);    
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //BUSCAMOS LA CARRERAS QUE EXISTEN
    obj.getCarreras = ()=>{
        console.log(obj.Alumno);
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getCarreras"}, historico : obj.historico, _idCampus : obj.Alumno._idCampus}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayCarreras = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //BUSCAMOS DATOS DE LA CARREA Y ASIGNATURA QUE EXISTEN
    obj.getCarrera =(e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getCarrera"}, _idCarrera : e, Alumno : obj._idAlumno }
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Carrera = res.data.Carrera;
                    obj.Asignaturas = res.data.Asignaturas;
                    obj.AgregarMaterias(e);

  
                    
            
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //BOTON DE REGRESAR AL EMNU PRINCIPAL
    obj.btnRegresar = ()=>{
        location.href="?mod=Alumnos";
    }
    //EDITAR DATOS DEL ALUMNO
    obj.btnEditarAlumno = () =>{
        obj.Alumno.opc="edit";
        console.log(obj.Calificaciones);
        $http({
            method: 'POST',
            url: url,
            data: {Alumnos: obj.Alumno, Calificaciones : obj.Calificaciones, Antecedentes : obj.Antecedente}
        }).then(function successCallback(res) {
   
            if (res.data.Bandera == 1) {           
                toastr.success(res.data.mensaje);  
                obj.getAlumno();
                obj.formEnabled  = true; 
               
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    //GRADOS QUE EXISTEN DEPENDE DE LA CARRERA
    obj.getGrados = (mod)=>{
        //        var array = [];
        //        for(let i = 0; i<mod;i++){
        //            array.push(i);
        //        }
                obj.setGrados(mod);
                return _.range(mod);
    }
    //LLENAR LOS GRADOS
    obj.setGrados = (mod)=>{
        if(mod<1){
            obj.Calificaciones.grados = [];
        }
        if(!obj.Calificaciones.grados){
            obj.Calificaciones.grados = [];
            for(let i = 0; i<mod;i++){
                obj.Calificaciones.grados[i] = [];
            }
        }
        while(mod< obj.Calificaciones.grados.length) obj.Calificaciones.grados.pop();
        while(mod> obj.Calificaciones.grados.length) obj.Calificaciones.grados.push([]);
        
    }
    //AGREGAR POR PRIMERA VEZ LA MATERIAS
    obj.AgregarMaterias = (e)=>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"SetCalificaciones" , Alumno : obj.Alumno}, Carrera : obj.Carrera,
                 Asignaturas : obj.Asignaturas, Calificaciones : obj.Calificaciones}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCalificaciones (e);
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.AgregarAntecedentes = (e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"setAntecedentes" } , _idAlumno : obj._idAlumno}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCalificaciones (e);
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });    
    }
    //BUSCAMOS LAS MATERIAS QUE EXITEN EN LA CARRERA
    obj.getCalificaciones = (e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getCalificaciones"}, Carrera : obj.Carrera, _idCarrera : e, Alumno : obj.Alumno}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.Calificaciones = res.data.Calificacion;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //GUARDAMOS CALIFICACION 
    obj.btnGuardarCalificacion = ()=>{
      
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"Calificaciones"}, Asignaturas : obj.Asignaturas , _idCarrera : obj.Carrera._id}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayAsiganturas = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }

    obj.getAntecedentes = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Alumnos: {opc:"getAntecedentes"}, _idAlumno : obj._idAlumno}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Antecedente = res.data.Data;
             
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
  
    //ESTA FUNCION INICIALIZA AL ENTRAR AL VIST
    angular.element(document).ready(function(){
                $(".calendario").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    maxDate: '+1w',
                    format: "yyyy-mm-dd",
                    weekStart:1,
                    calendarWeeks: true,
                    language:"es",
                    autoclose: true
        });
        obj.getAlumno ();
        obj.getCarreras();
        obj.getTipo_Observaciones();
        obj.AgregarAntecedentes();
        obj.getAntecedentes();
        obj.formEnabled = true;

     });
}
