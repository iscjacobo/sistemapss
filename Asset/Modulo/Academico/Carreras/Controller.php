<?php

    (@__DIR__ == '__DIR__') && define('__DIR__',  realpath(dirname(__FILE__)));
        function get_template($form='principal'){
            $file = __DIR__.'/Html/Carreras_'.$form.'.html';
            $template = file_get_contents($file);
            return $template;
        };
        function retorna_vista($vista,$data=array()){
            switch($vista){
                case 'principal':
                case 'nuevo':
                case 'editar':
                    $html = get_template($vista);
                    $html = str_replace("{id}", $data["id"], $html);
                    $html = str_replace("{rol}", $data["rol"], $html);
                break;
            }
            print $html;
        };
    function principal(){
        $opc = isset($_GET['opc'])? htmlspecialchars($_GET['opc']):"principal";
        
        switch($opc){
            case 'principal':
            case 'nuevo':
            case 'editar':
                $data["id"] = isset($_GET["id"])? htmlspecialchars($_GET["id"]):$_SESSION["_idCampus"];
                $data["rol"] = $_SESSION["rol"];
                retorna_vista($opc,$data);
            break; 
            
    }
    }

    principal();