<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Carreras
 *
 * @author Manuel Jacobo
 */


error_reporting(E_ALL);
ini_set('display_errors', '1');
session_name("loginTitulos");
session_start();
include ("../../../../../Asset/Clases/ConexionMySQL.php");
include ("../../../../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');


class Carreras {
    //put your code here
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    public function principal(){
        $this->formulario = json_decode(file_get_contents('php://input'));
        switch ($this->formulario->Carreras->opc){
                case 'new':
                case 'edit':
                case 'delete':
                case 'Estatus':
                        if($this->setCarreras()){
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["mensaje"]=$this->formulario ->Carreras->opc == 'new'? 
                                    "La Carrera se registro satisfactoriamente":"La Carrera se edito satisfactoriamente";
                            if($this->formulario ->Carreras->opc == 'edit'){
                                $this->setAsignaturas();
                            }
                        }else{
                            $this->jsonData["Bandera"]=0;
                            $this->jsonData["mensaje"]= $this->formulario ->Carreras->opc == 'new'? 
                                    "Error al registrar la Institucion":"Error al editar la Institucion";
                        }
                break;
                case 'getInstituciones':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getInstituciones();
                            $this->jsonData["mensaje"]="Listo";            
                break;
                case 'Campus':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCampus();
                            $this->jsonData["mensaje"]="Listo";
                break;
                case 'get':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getCarreras();
                break;
                case 'getCarrera':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Carrera"] = $this->getCarrera();
                            $this->jsonData["Asignaturas"] = $this->getAsignaturas();
                break;
                case 'getT':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getTipo_Asignaturas();
                break;
                case 'getTipoPeriodo':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getTipoPeriodo();
                            $this->jsonData["Reconocimiento"] = $this->getAutorizacion();
                break;
                case 'getNivelEstudios':
                            $this->jsonData["Bandera"]=1;
                            $this->jsonData["Data"] = $this->getNivelEstudios();
                break;
                case 'DeleteMateria':
                        $this->jsonData["Bandera"]=1;
                        $this->jsonData["Data"] = $this->DeleteMateria();
                    break;

             
        }
        
        print json_encode($this->jsonData);
        
    }
 
    private function setCarreras(){
       $fecha = date("Y-m-d");
       switch ($this->formulario ->Carreras->opc){

            case 'new':
                $_idCampus = $this->formulario->Campus->id;
                $_idCarrera = $this->formulario->Carreras->_idCarrera;
                $sql ="INSERT INTO Carreras(
                                   Carrera, Rvoe, FechaExpedicionRVOE, cveCarrera, _idCampus,
                                   USRCreacion, FechaCreacion, USRModifico, FechaModifico,
                                   Estatus, _idCarrera, Modulos, _idNivelEstudios, _idTipoPeriodo,
                                   CalificacionMaxima, CalificacionMinima, CalificacionMinimaAprobatoria,
                                   ClavePlan, _idAutorizacionReconocimiento
                                   )
                            values (
                                   UPPER( '{$this->formulario->Carreras->Carrera}'),
                                  '{$this->formulario->Carreras->Rvoe}',
                                  '{$this->formulario->Carreras->FechaExpedicionRVOE}',
                                  '{$this->formulario->Carreras->cveCarrera}',
                                  '$_idCampus',
                                  '{$_SESSION["usr"]}',
                                  '$fecha',
                                  '{$_SESSION["usr"]}',
                                  '$fecha',
                                  '1',
                                  '$_idCarrera',
                                  '{$this->formulario->Carreras->Modulos}',
                                  '{$this->formulario->Carreras->_idNivelEstudios}',
                                  '{$this->formulario->Carreras->_idTipoPeriodo}',
                                  '{$this->formulario->Carreras->CalificacionMaxima}',
                                  '{$this->formulario->Carreras->CalificacionMinima}',
                                  '{$this->formulario->Carreras->CalificacionMinimaAprobatoria}',
                                  '{$this->formulario->Carreras->ClavePlan}',
                                  '{$this->formulario->Carreras->_idAutorizacionReconocimiento}'
                                  )";           
                break;
           case 'edit':
                        $sql = "UPDATE Carreras SET "
                        ."_idCarrera =  UPPER('{$this->formulario->Carrera->_idCarrera}'),"
                        ."Carrera =  UPPER('{$this->formulario->Carrera->Carrera}'),"
                        ."Rvoe =  '{$this->formulario->Carrera->Rvoe}',"
                        ."FechaExpedicionRVOE =  '{$this->formulario->Carrera->FechaExpedicionRVOE}',"
                        ."cveCarrera =  '{$this->formulario->Carrera->cveCarrera}',"
                        ."USRModifico = '{$_SESSION["usr"]}',"
                        ."FechaModifico= '$fecha',"
                        ." Modulos = '{$this->formulario->Carrera->Modulos}',"
                        ." _idNivelEstudios = '{$this->formulario->Carrera->_idNivelEstudios}',"       
                        ." _idTipoPeriodo = '{$this->formulario->Carrera->_idTipoPeriodo}',"
                        ." CalificacionMaxima = '{$this->formulario->Carrera->CalificacionMaxima}',"
                        ." CalificacionMinima = '{$this->formulario->Carrera->CalificacionMinima}',"
                        ." CalificacionMinimaAprobatoria = '{$this->formulario->Carrera->CalificacionMinimaAprobatoria}',"
                        ." ClavePlan = '{$this->formulario->Carrera->ClavePlan}',"
                        ." _idAutorizacionReconocimiento = '{$this->formulario->Carrera->_idAutorizacionReconocimiento}'"
                       . " where _id = '{$this->formulario->Carrera->_id}'";
              break;
                case 'Estatus':

                    $status = $this->formulario->historico == "true"? 1:0;
                       $sql = "UPDATE Carreras SET "
                      ."Estatus = '{$status}',"
                      ."USRModifico = '{$_SESSION["usr"]}',"
                      ."FechaModifico= '$fecha'"
                      . " where _id = '{$this->formulario->_idCarrera}'";

               break;
                            
          
        }
        return $this->conn->query($sql)? true: false;
    }

    private function getCarreras(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];
        $status = $this->formulario->historico  == "true"? 0:1;
        $_idCampus = $_SESSION["_idCampus"];
                
                        $sql = "SELECT  _idCarrera, _id, Carrera, Modulos, _idCampus, Estatus FROM Carreras WHERE Estatus = '$status'
                                AND  _idCampus = '{$this->formulario->Campus->id}'";
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                        array_push($array, $row);
                        }
               
        return $array;
    }

    private function getCarrera(){
        $_idCarrera = $this->formulario->_idCarrera->id;
        $sql = "SELECT * FROM Carreras WHERE _id = $_idCarrera";
   
        return $this->conn->fetch($this->conn->query($sql));
    }
    
    private function getTipo_Asignaturas(){
        $array = array();
        $sql = "SELECT * FROM Catalogo_Tipo_Asignatura";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
    
    private function getAsignaturas(){
        /*Armar el arreglo de las materias*/
        $a = array();
        $array = array("grados"=>array());
        
        for($i = 0; $i < $this->jsonData["Carrera"]["Modulos"]; $i++){ //recorre cuantos modulos tiene la carrera (bimrestre, trimestre etc)
            $sql = "SELECT * FROM Asignaturas where _id_carrera = {$this->formulario->_idCarrera->id} and modulo= ". ($i+1) . " order by id_Asignatura";
            $id = $this->conn->query($sql);
            if($this->conn->count_rows()!=0){
                $a = array();
                while($row  = $this->conn->fetch($id)){
                    array_push($a, array("materia"=>$row));
                }
                $array["grados"][$i] = $a;
            }else{
                $array["grados"][$i] = array();
            }
            
        }
        //var_dump($array);
        return $array;
        
    }
    
    private function setAsignaturas(){
        $fecha = date("Y-m-d");
        foreach ($this->formulario->Asignaturas->grados as $key => $value){
            foreach ($value as $materia){
                //var_dump($materia->materia->Asignatura);
                $sql = "SELECT _id FROM Asignaturas where id_Asignatura = '{$materia->materia->id_Asignatura}'";
                $id = $this->conn->query($sql);
                if($this->conn->count_rows()!=0){
                    //Entrara a modificar el registro de la base de datos
                    $row = $this->conn->fetch($id);
                    //var_dump($row);
                    $sql = "UPDATE Asignaturas SET id_Asignatura='{$materia->materia->id_Asignatura}', Asignatura='{$materia->materia->Asignatura}', "
                            . "Clave_asignatura = '{$materia->materia->Clave_asignatura}', _id_tipoAsignatura = '{$materia->materia->_id_tipoAsignatura}', "
                            . "Creditos='{$materia->materia->Creditos}', USREdicion='{$_SESSION["usr"]}', FechaEdicion='{$fecha}' "
                            . "where _id = '{$row["_id"]}'";
                }else{
                    //si no tiene _id se realizara un registro nuevo
                    $sql = "INSERT INTO Asignaturas (id_Asignatura, modulo, Asignatura, _id_carrera, Clave_asignatura, _id_tipoAsignatura, Creditos, USRCreacion, FechaCreacion, USREdicion, FechaEdicion) "
                            . "values ('{$materia->materia->id_Asignatura}','{$materia->materia->modulo}','{$materia->materia->Asignatura}','{$this->formulario->Carrera->_id}',"
                            . "'{$materia->materia->Clave_asignatura}','{$materia->materia->_id_tipoAsignatura}','{$materia->materia->Creditos}','{$_SESSION["usr"]}','{$fecha}','{$_SESSION["usr"]}','{$fecha}')";
                }
                $this->conn->query($sql);
            }
        }
        return true;
    }
    private function DeleteMateria (){
        $sql = "DELETE from Asignaturas where _id=".$this->formulario->_id;
        return $this->conn->query($sql);
    }
    
    private function getTipoPeriodo(){
        $array = array();
        $sql = "SELECT * FROM Catalogo_tipo_periodo";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getNivelEstudios(){
        $array = array();
        $sql = "SELECT * FROM Catalogo_Nivel_De_Estudios";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }

    private function getCampus(){
        $array = array();
        $sql = "";
        $rol = $_SESSION["rol"];
        $_idInstitucion = $_SESSION["_idInstitucion"];
                switch ("$rol") {
                    case 'root':
                       $sql="SELECT _id, Nombre from Campus WHERE _idInstitucion = '{$this->formulario->Instituto->_idInstituto}' and Estatus = 1";
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                        break;

                    case 'Admin';
                    case 'CtrlEsc':
                        $sql="SELECT _id, Nombre FROM Campus WHERE _idInstitucion = ' $_idInstitucion' AND Estatus = 1 ";    
                        $id = $this->conn->query($sql);
                        while($row = $this->conn->fetch($id)){
                         array_push($array, $row);
                        }  
                    break;

                                  
                    
            }
        return $array;
    }
    private function getAutorizacion(){
        $array = array();
        $sql = "SELECT * FROM Autorizacion_reconocimiento";
        $id = $this->conn->query($sql);
        while($row = $this->conn->fetch($id)){
            array_push($array, $row);
        }
        return $array;
    }
   
    
    private function getInstituciones(){

        $array = array();
        $sql = "SELECT _id, Institucion FROM Instituciones where Estatus = '1'";
         $id = $this->conn->query($sql);
         while($row = $this->conn->fetch($id)){
          array_push($array, $row);
         }  
         return $array;

     }
     
};

$app = new Carreras($array_principal);
$app->principal();