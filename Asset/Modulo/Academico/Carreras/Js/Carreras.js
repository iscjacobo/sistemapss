/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var url = "./Modulo/Academico/Carreras/Ajax/Carreras.php";
tituloElectronico
                 .controller('CarrerasCtrl',['$scope','$http',CarrerasCtrl])
                 .controller('EditarCarreraCtrl',['$scope','$http', EditarCarreraCtrl]);
function CarrerasCtrl($scope,$http){
    var obj = $scope;
    obj.arrayInstituciones =[];
    obj.Instituto = {};

    obj.arrayCampus =[];
    obj.Campus = {};

    obj.arrayCarreras =[];
    obj.Carrera = {
       
    };
  
    obj.btnShow = false; 
    obj.rol = "";
    obj.formEnabled = false;
    obj.TipoPeriodo;
    obj.NivelEstudios;
    
    obj.arrayAutorizacion;
    obj.historico = false;
    
    //Limpiar formulario
    obj.btnNuevo = () =>{
        obj.Carrera = {}
        
         obj.formEnabled = false;
    }
    //Traer los compus
    obj.getCampus = ()=>{
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"Campus"}, Instituto : obj.Instituto}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayCampus = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Regresara carreras
    obj.btnRegresar = ()=>{
        location.href="?mod=Carreras";
    }
    //Mandar ID a la vista editar
    obj.btnEditar = (e) =>{

        window.location.href = "?mod=Carreras&opc=editar&id="+e;
    }
    //Traer las carres de cada sucursal
    obj.getCarreras = ()=>{
     
        console.log(obj.historico);
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"get"}, Campus: obj.Campus, historico : obj.historico}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.arrayCarreras = res.data.Data;
                    obj.btnShow = (obj.rol != "CtrlEsc")?true:false; 
                    


                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Guardar carrera
    obj.btnGuardarCarrera = ()=>{
        obj.Carrera.opc="new";
        obj.Carrera.FechaExpedicionRVOE = $("#txtFechaExpedicionRVOE").val();
        console.log(obj.Carrera);
        $http({
            method: 'POST',
            url: url,
            data: {Carreras: obj.Carrera, Campus: obj.Campus}
        }).then(function successCallback(res) {
            console.log(res.data);
            if (res.data.Bandera == 1) {           
                toastr.success(res.data.mensaje);  
              
                obj.formEnabled  = true; 
               
            } else {
                toastr.error(res.data.mensaje);
            }
        }, function errorCallback(res) {
        });
    }
    //Dar de baja una sucursal
    obj.btnBaja = (e)=>{
        if(confirm("Estas seguro de dar de baja la carrera")){
            console.log(e);
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"Estatus"}, historico : obj.historico , _idCarrera: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCarreras();
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    }
    //dar de alta una sucursal
    obj.btnAlta = (e)=>{
        if(confirm("¿Estas seguro de dar de alta la carrera?")){
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"Estatus"}, historico : obj.historico , _idCarrera: e}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.getCarreras();
                }else{
                console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
      }
    }
    
    obj.getTipoPeriodo = ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getTipoPeriodo"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.TipoPeriodo = res.data.Data;
                    obj.arrayAutorizacion = res.data.Reconocimiento;
         
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });

  
    }
    obj.getNivelEstudios = ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getNivelEstudios"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.NivelEstudios = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getInstituciones = () =>{
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getInstituciones"}}
            }).then(function successCallback(res){

                if(res.data.Bandera == 1){
                    obj.arrayInstituciones = res.data.Data;
                 
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    //Funcion que se inizaliza sola
    angular.element(document).ready(function(){
        
        obj.getNivelEstudios();
        obj.getTipoPeriodo();
        
      
        switch(obj.rol){
            case 'root':
                    obj.getInstituciones();
                break;
            case 'Admin':
                obj.getCampus();
                break;
            case 'CtrlEsc':
                obj.btnShow = true;
                
                obj.getCarreras();
                break;

   
           
        }
         $(".calendario").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            maxDate: '+1w',
                            format: "yyyy-mm-dd",
                            weekStart:1,
                            calendarWeeks: true,
                            language:"es",
                            autoclose: true
            });
     });
   
}
function EditarCarreraCtrl($scope,$http){
    var obj = $scope;
    obj.editCarrera = {};
    obj._idCarrera = {};
    obj.Asignaturas = {};
    obj.Tipo_Asignaturas = [];
    obj.TipoPeriodo;
    obj.NivelEstudios;
    obj.editar = true;
    obj.rol="";
    obj.arrayAutorizacion;

    //Traer la carrera a editar
    obj.getCarrera = (e) =>{
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getCarrera"}, _idCarrera : obj._idCarrera}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.editCarrera = res.data.Carrera;
                    $("#txtfecheExpedicionRVOE").val(obj.editCarrera.FechaExpedicionRVOE)
                    obj.Asignaturas = res.data.Asignaturas;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    
    obj.getTipo_Asignatura = ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getT"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.Tipo_Asignaturas = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
  
    //Guardar datos 
    obj.btnEditarCarrera = () =>{
        obj.editCarrera.FechaExpedicionRVOE = $("#txtfecheExpedicionRVOE").val();
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"edit"},  Carrera : obj.editCarrera, Asignaturas: obj.Asignaturas}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    toastr.success(res.data.mensaje);  
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });

    }
    
    obj.btnEliminarMateria = (materia, grado)=>{
        if(materia.materia._id!=""){
            if(confirm("¿Estas seguro de borrar la materia?")){
                $http({
                    method: 'POST',
                        url: url,
                        data: {Carreras: {opc:"DeleteMateria"},  _id: materia.materia._id}
                    }).then(function successCallback(res){
                        if(res.data.Bandera == 1){
                            toastr.success(res.data.mensaje);  
                        }else{
                           console.log(res.data.mensaje);
                        }
                    }, function errorCallback(res){
                });
            }
        }
        var i = obj.Asignaturas.grados[grado].indexOf(materia)
        obj.Asignaturas.grados[grado].splice( i, 1 );
    }
    //regresar
    obj.btnRegresar = ()=>{
        location.href="?mod=Carreras";
    }
    obj.setGrados = (mod)=>{
        if(mod<1){
            obj.Asignaturas.grados = [];
        }
        if(!obj.Asignaturas.grados){
            obj.Asignaturas.grados = [];
            for(let i = 0; i<mod;i++){
                obj.Asignaturas.grados[i] = [];
            }
        }
        while(mod< obj.Asignaturas.grados.length) obj.Asignaturas.grados.pop();
        while(mod> obj.Asignaturas.grados.length) obj.Asignaturas.grados.push([]);
        
    }
    
    obj.btnagregarAsignatura = (mod)=>{
        var materia = {materia:{_id:"", id_Asignatura:"",Asignatura:"",Clave_asignatura:"",Creditos:"",_id_tipoAsignatura:"", modulo:(mod+1)}}
        obj.Asignaturas.grados[mod].push(materia);
        console.log(obj.Asignaturas)
    }
    
    obj.getGrados = (mod)=>{
//        var array = [];
//        for(let i = 0; i<mod;i++){
//            array.push(i);
//        }
        //obj.setGrados(mod);
        return _.range(mod);
    }
    
    obj.getTipoasignatura = (id)=>{
        var tempid = obj.Tipo_Asignaturas.find(e=>e.idTipoAsignatura === id)
        return tempid != undefined? tempid.Descripcion:false;
    }
    
    obj.getTipoPeriodo = ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getTipoPeriodo"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.TipoPeriodo = res.data.Data;

                    obj.arrayAutorizacion = res.data.Reconocimiento;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }
    obj.getNivelEstudios = ()=>{
        
        $http({
            method: 'POST',
                url: url,
                data: {Carreras: {opc:"getNivelEstudios"}}
            }).then(function successCallback(res){
                if(res.data.Bandera == 1){
                    obj.NivelEstudios = res.data.Data;
                }else{
                   console.log(res.data.mensaje);
                }
            }, function errorCallback(res){
        });
    }

    angular.element(document).ready(function(){
        $(".calendario").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            maxDate: '+1w',
                            format: "yyyy-mm-dd",
                            weekStart:1,
                            calendarWeeks: true,
                            language:"es",
                            autoclose: true
            });
        obj.getCarrera();
        obj.getTipo_Asignatura();
        obj.getNivelEstudios();
        obj.getTipoPeriodo();
        obj.editar = obj.rol!="CtrlEsc"? true:false;
        obj.boton = obj.rol!="CtrlEsc"? false:true;
        
     });

}