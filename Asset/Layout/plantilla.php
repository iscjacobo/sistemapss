
<!doctype html>
<html lang="es" >
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="refresh" content="<?php echo $duracionsession*60;?>;URL='<?php echo $_SERVER['PHP_SELF'];?>'">
    <title>Sistema de Titulacion Electronico</title>
    <link rel="stylesheet" type = "text/css" href="./Css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./Css/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./Css/dist/css/skins/skin-icep.css">
    <link rel="stylesheet" href="./Includes/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <!--<link rel="stylesheet" href="./includes/plugins/daterangepicker/daterangepicker-bs3.css">-->
    <!-- bootstrap wysihtml5 - text editor -->
    <!--<link rel="stylesheet" href="./includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">-->
    <!-- DataTables -->
    <link rel="stylesheet" href="./Includes/plugins/datatables/dataTables.bootstrap.css">
    <!-- Pace page -->
    <link rel="stylesheet" href="./Includes/plugins/pace/pace.min.css" />
    <!-- time Picker -->
    <link rel="stylesheet" href="./Includes/plugins/timepicker/bootstrap-timepicker.min.css" />
    <!-- swictchery -->
<!--    <link rel="stylesheet" href="./Includes/plugins/switchery/dist/switchery.min.css" />-->
    <!--switchBootstrap-->
    <!-- switch -->
    <link rel="stylesheet" href="./Includes/plugins/switchbootstrap4/switch.css">
    <!-- CSS personalizados -->
    <link rel="stylesheet" href="./Css/styles.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="./Includes/plugins/select2/select2.min.css">
    <!--toastr -->
    <link rel="stylesheet" href="./Includes/plugins/toastr/toastr.scss">
    <!--Angular Material -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.12/angular-material.min.css">
    <?php
        switch($mod){
            case 'perfil':
                echo "<link rel='stylesheet' href='./Modulo/perfil/css/perfil.css'>";
                break;
            
            case 'Sucursales':
                echo "<link rel='stylesheet' href='./Modulo/Catalogos/Sucursales/Css/Css.css'>";
                break;
        }
    ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-icep sidebar-mini fixed" ng-app="tituloElectronico">
    
    <div class="wrapper">
      <?php include( "./Includes/cabezera.php") ?>
      
      <?php include("./Menus/menu.php");?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php
            
            if (file_exists($path_modulo))include($path_modulo);
            else die ('Error al cargar el modulo <b>'.$modulo.'</b>. No existe el archivo <b>'.$conf[$modulo]['archivo'].'</b>');
	?>
      </div><!-- /.content-wrapper -->
      
        <?php include("./Includes/pie.html");?>
        <?php include("./Includes/siderbar.php");?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    
    <!-- Angular -->
<!--    <script src="./Js/angular/angular.min.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.6/angular.min.js"></script>
    <!-- Angular Material Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.19/angular-material.min.js"></script>
    <script src="./Js/angular/app.js"></script>
    <script type="text/javascript" src="./Js/funciones.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="./Includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!--underscore.js -->
      <script src =  "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js" > </script> 
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Jquery validate -->
    <script type="text/javascript" src="./Js/jquery.validate.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="./Js/bootstrap.min.js"></script>
    <!-- jquery.ba-throttle-debounce -->
    <script type="text/javascript" src='./Includes/plugins/jquery.ba-throttle-debounce/jquery.ba-throttle-debounce.min.js'></script>
    <!-- Google maps-->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4qtAZB68QQCueu-EDupocWSRgdOg_JMQ"></script>-->
    <!-- Google maps-->
<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>-->
    <!-- Morris.js charts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="./includes/plugins/morris/morris.min.js"></script>-->
    <!-- Sparkline -->
<!--    <script src="./Includes/plugins/sparkline/jquery.sparkline.min.js"></script>-->
    <!-- jvectormap -->
<!--    <script src="./Includes/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="./Includes/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
    <!-- jQuery Knob Chart -->
<!--    <script src="./Includes/plugins/knob/jquery.knob.js"></script>-->
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="./Includes/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="./Includes/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
<!--    <script src="./Includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
    <!-- Slimscroll -->
    <script src="./Includes/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./Includes/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./Css/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) 
    <script src="./css/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
<!--    <script src="./Css/dist/js/demo.js"></script>-->
    <!-- DataTables -->
    <script src="./Includes/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="./Includes/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Time Picker -->
<!--    <script src='./Includes/plugins/timepicker/bootstrap-timepicker.min.js'></script>-->
    <!-- pace page -->
    <script type="text/javascript" src='./Includes/plugins/pace/pace.min.js'></script>
    <!-- numeric -->
    <script type="text/javascript" src='./Includes/plugins/numeric/jquery.numeric.js'></script>
    <!-- push -->
    <script type="text/javascript" src='./Includes/plugins/push/push.min.js'></script>
    <!-- Switchery -->
<!--    <script src="./Includes/plugins/switchery/dist/switchery.min.js"></script>-->
    <!-- jquery validate -->
    <script type="text/javascript" src='./Includes/plugins/validate/jquery.validate.js'></script>
    <!-- Select2 -->
        <script src="./Includes/plugins/select2/select2.full.min.js"></script>
    <!-- Custom theme scrits-->
    <script type="text/javascript" src="./Js/custom.js"></script>
    
    <!-- toastr -->
    <script type="text/javascript" src='./Includes/plugins/toastr/toastr.js'></script>
    <script type="text/javascript">
        $(document).ajaxStart(function() { Pace.restart(); });
    </script>
    <?php
        /*Importacion de los archivos Js que llevan angular */
        switch($mod){
            case 'Entidades':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/EntidadesFederativas/Js/Entidades.js'></script>";
            break;
            case 'Firmantes':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/FirmantesAutorizados/Js/Firmantes.js'></script>";
            break;    
            case 'Nivel':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/NivelDeEstudios/Js/NivelEstudios.js'></script>";
            break;    
            case 'Antecedentes':
            echo "<script type='text/javascript' src='./Modulo/Catalogos/AntecedentesEstudio/Js/Antecedentes.js'></script>";
        break;  
            case 'Fundamento':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/FundamentoLegal/Js/FundamentoLegal.js'></script>";
            break;    
            case 'Modalidad':
            echo "<script type='text/javascript' src='./Modulo/Catalogos/ModalidadDeTitulacion/Js/ModalidadDeTitulacion.js'></script>";
        break; 
            case 'Autorizacion':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/AutorizacionDeReconocimiento/Js/Autorizacion.js'></script>";
            break;   
            case 'Cancelacion':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/MotivosDeCancelacion/Js/Cancelacion.js'></script>";
                break;
            case 'Genero':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/Genero/Js/Genero.js'></script>";
            break;   
            case 'Periodo':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/Periodo/Js/Periodo.js'></script>";
            break;   
            case 'Certificacion':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/Certificacion/Js/Certificacion.js'></script>";
            break;   
            case 'Observaciones':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/Observaciones/Js/Observaciones.js'></script>";
            break;
            case 'Asignatura':
            echo "<script type='text/javascript' src='./Modulo/Catalogos/Asignatura/Js/Asignatura.js'></script>";
            case 'Documentos':
                echo "<script type='text/javascript' src='./Modulo/Catalogos/$mod/Js/$mod.js'></script>";    
                break;
            break;
            case 'Instituciones':
            case 'Sucursales':
            case 'Usuarios':
                echo "<script type='text/javascript' src='./Modulo/Configuracion/$mod/Js/$mod.js'></script>";
            break;
            case 'Carreras':
                echo "<script type='text/javascript' src='./Modulo/Academico/$mod/Js/$mod.js'></script>";
            break;
            case 'Alumnos':
                echo "<script type='text/javascript' src='./Modulo/Academico/$mod/Js/$mod.js'></script>";
            break;
            
            case 'Registro':
            case 'Ecertificado':
                echo "<script type='text/javascript' src='./Modulo/TitulosElectronicos/$mod/Js/$mod.js'></script>";
            break;
        }
    ?>
    
  </body>
</html>