<?php
(@__DIR__ == '__DIR__') && define('__DIR__',  realpath(dirname(__FILE__)));
$html = "";
$opcMenu = array();
$sum = 0;
$seccion = "";
$faicons = array("Titulos electronicos"=>"fa-book","Colegiatura"=>"fa-dollar","Control"=>"fa-group","Reportes"=>" fa-book","Academico"=>"fa-mortar-board","Historico"=>"fa-calendar",
                 "Financiero"=>"fa-line-chart","CRM"=>"fa-user","Catalogos"=>"fa-book", "Configuracion"=>"fa-cogs");
$file = __DIR__.'/menu.html';
$template = file_get_contents($file);
$template = str_replace('{imagen}', $img, $template);
$template = str_replace('{Nombre}', $_SESSION["nombrecorto"], $template);


/*SE genera el menu apartir de esete modulo*/
    switch($_SESSION["rol"]){
        
        case 'root':
            /*Seccion de control */
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Alumnos";                      $opcMenu[$sum]["opc"]="?mod=Alumnos";          $sum++;
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Carreras";                     $opcMenu[$sum]["opc"]="?mod=Carreras";          $sum++;
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Grupos";                       $opcMenu[$sum]["opc"]="?mod=Personal";          $sum++;
            
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Entidades Federativas";        $opcMenu[$sum]["opc"]="?mod=Entidades";         $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Firmantes Autorizados";        $opcMenu[$sum]["opc"]="?mod=Firmantes";         $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Estudio antecedente";          $opcMenu[$sum]["opc"]="?mod=Antecedentes";        $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Nivel de estudios  ";          $opcMenu[$sum]["opc"]="?mod=Nivel";             $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Fundamento Legal";             $opcMenu[$sum]["opc"]="?mod=Fundamento";        $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Modalidad de tituacion";       $opcMenu[$sum]["opc"]="?mod=Modalidad   ";      $sum++;            
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Autorizacion Reconocimiento";  $opcMenu[$sum]["opc"]="?mod=Autorizacion";      $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Motivos de Cancelacion";       $opcMenu[$sum]["opc"]="?mod=Cancelacion";       $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Genero";                       $opcMenu[$sum]["opc"]="?mod=Genero";            $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Periodo";                      $opcMenu[$sum]["opc"]="?mod=Periodo";           $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Certificacion";                $opcMenu[$sum]["opc"]="?mod=Certificacion";     $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Observaciones";                $opcMenu[$sum]["opc"]="?mod=Observaciones";     $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Asignatura";                   $opcMenu[$sum]["opc"]="?mod=Asignatura";        $sum++;
            $opcMenu[$sum]["grupo"]="Catalogos";                  $opcMenu[$sum]["titulo"]= "Documentos";                   $opcMenu[$sum]["opc"]="?mod=Documentos";        $sum++;
          
            $opcMenu[$sum]["grupo"]="Configuracion";              $opcMenu[$sum]["titulo"]= "Instituciones";                $opcMenu[$sum]["opc"]="?mod=Instituciones";     $sum++;
            $opcMenu[$sum]["grupo"]="Configuracion";              $opcMenu[$sum]["titulo"]= "Sucursales";                   $opcMenu[$sum]["opc"]="?mod=Sucursales";        $sum++;
            $opcMenu[$sum]["grupo"]="Configuracion";              $opcMenu[$sum]["titulo"]= "Usuarios";                     $opcMenu[$sum]["opc"]="?mod=Usuarios";          $sum++;
            
             /*Seccion Titulos*/
             $opcMenu[$sum]["grupo"]="Titulos electronicos";      $opcMenu[$sum]["titulo"]= "Registro";                    $opcMenu[$sum]["opc"]="?mod=Registro";        $sum++;          
             $opcMenu[$sum]["grupo"]="Titulos electronicos";      $opcMenu[$sum]["titulo"]= "Generar XML";                 $opcMenu[$sum]["opc"]="?mod=XML";             $sum++;
             

            break;

        case 'Admin':
            /*Seccion Titulos*/
            $opcMenu[$sum]["grupo"]="Titulos electronicos";       $opcMenu[$sum]["titulo"]= "Registro";                     $opcMenu[$sum]["opc"]="?mod=Registro";        $sum++;          
            $opcMenu[$sum]["grupo"]="Titulos electronicos";       $opcMenu[$sum]["titulo"]= "Generar XML";                  $opcMenu[$sum]["opc"]="?mod=XML";             $sum++;
             
            /*Seccion Academica*/
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Alumnos";                      $opcMenu[$sum]["opc"]="?mod=Alumnos";           $sum++;          
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Carreras";                     $opcMenu[$sum]["opc"]="?mod=Carreras";          $sum++;
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Grupos";                       $opcMenu[$sum]["opc"]="?mod=Grupos";            $sum++;     
            $opcMenu[$sum]["grupo"]="Configuracion";              $opcMenu[$sum]["titulo"]= "Sucursales";                   $opcMenu[$sum]["opc"]="?mod=Sucursales";        $sum++;
            $opcMenu[$sum]["grupo"]="Configuracion";              $opcMenu[$sum]["titulo"]= "Usuarios";                     $opcMenu[$sum]["opc"]="?mod=Usuarios";          $sum++;
           
            break;
        
        case 'CtrlEsc':
            /*Seccio de control escolar|*/
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Alumnos";                      $opcMenu[$sum]["opc"]="?mod=Alumnos";           $sum++;          
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Carreras";                     $opcMenu[$sum]["opc"]="?mod=Carreras";          $sum++;
            $opcMenu[$sum]["grupo"]="Academico";                  $opcMenu[$sum]["titulo"]= "Grupos";                       $opcMenu[$sum]["opc"]="?mod=Personal";          $sum++;
            
            $opcMenu[$sum]["grupo"]="Titulos electronicos";       $opcMenu[$sum]["titulo"]= "E-Certificado";                $opcMenu[$sum]["opc"]="?mod=Ecertificado";       $sum++;  
            $opcMenu[$sum]["grupo"]="Titulos electronicos";       $opcMenu[$sum]["titulo"]= "E-Titulacion";                 $opcMenu[$sum]["opc"]="?mod=Etitulacion";        $sum++;  
            break;
        
    }
    $conM = count($opcMenu);
    $tempMod = "?mod=$mod";
    $grupo = "";
    
    /*Recorremos el arreglo para seber cual grupo debemos de activar*/
    if($mod != "home"){
        for($i=0;$i<=$conM;$i++){
            if(strcmp($tempMod,$opcMenu[$i]["opc"])==0){
                $grupo = $opcMenu[$i]["grupo"];
                break;
            }
        }
    }
    
    
    for($x=0;$x<=$conM;$x++){
        if(isset($opcMenu[$x]["grupo"])){
            if ($seccion != $opcMenu[$x]["grupo"]){
                if($seccion != ""){
                    $html .= "</ul></li>";
                }
                $active = strcmp($grupo, $opcMenu[$x]["grupo"])==0? "active":"";
                $seccion =  $opcMenu[$x]["grupo"];
                $html .= "<li class=' $active treeview'>
                            <a href='#'>
                              <i class='fa {$faicons[$seccion]}'></i> <span>$seccion</span> <i class='fa fa-angle-left pull-right'></i>
                            </a>
                            <ul class='treeview-menu'>";
            }
            $active = strcmp($tempMod,$opcMenu[$x]["opc"])==0? "active":"";
            $html .="<li class='$active'><a href='{$opcMenu[$x]["opc"]}'><i class='fa fa-circle-o'></i>{$opcMenu[$x]["titulo"]} </a></li>";
        }
    }
    $html .= "</ul></li>";
    
    $template = str_replace('{menus}',$html,$template);
/*Fin de la genarcaion del mene*/
print $template;