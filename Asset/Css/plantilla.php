<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<title>Sistema CRM ventas</title>
	
	<link rel="stylesheet" type = "text/css" href="./css/bootstrap.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./css/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="./css/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="./includes/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <!--<link rel="stylesheet" href="./includes/plugins/morris/morris.css">-->
    <!-- jvectormap 
    <link rel="stylesheet" href="./includes/plugins/jvectormap/jquery-jvectormap-1.2.2.css">-->
    <!-- Date Picker -->
    <link rel="stylesheet" href="./includes/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="./includes/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="./includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="./includes/plugins/datatables/dataTables.bootstrap.css">
    <!-- time Picker -->
    <link rel="stylesheet" href="./includes/plugins/timepicker/bootstrap-timepicker.min.css" />
    
    <!-- CSS personalizados -->
    <?php
        switch($mod){
            case 'newaspirante':
                echo "<link rel='stylesheet' href='./css/newaspirantes.css'";
            break;
            case 'detalles':
                echo "<link rel='stylesheet' href='./css/detalles.css'>";
            break;
            case 'inscribir':
                echo "<link rel='stylesheet' href='./css/inscripcion.css'>";
                break;
        }
    ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
<!--	<script type="text/javascript" src="./librerias/jquery.js"></script>
	<script type="text/javascript" src="./librerias/bootstrap.js"></script>
	<script type="text/javascript" src="./librerias/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="./librerias/javascripts.js"></script>-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <?php include( "./includes/cabezera.php") ?>
      
      <?php include("./menus/menu.php");?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php
            if (file_exists($path_modulo))include($path_modulo);
            else die ('Error al cargar el modulo <b>'.$modulo.'</b>. No existe el archivo <b>'.$conf[$modulo]['archivo'].'</b>');
	?>
      </div><!-- /.content-wrapper -->
      
        <?php include("./includes/pie.html");?>
        <?php include("./includes/siderbar.php");?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="./includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Jquery validate -->
    <script type="text/javascript" src="./js/jquery.validate.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="./js/bootstrap.min.js"></script>
    <!-- Google maps-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <!-- Morris.js charts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="./includes/plugins/morris/morris.min.js"></script>-->
    <!-- Sparkline -->
    <script src="./includes/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="./includes/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="./includes/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="./includes/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="./includes/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="./includes/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="./includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="./includes/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="./includes/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./css/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) 
    <script src="./css/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="./css/dist/js/demo.js"></script>
    <!-- DataTables -->
    <script src="./includes/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="./includes/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Time Picker -->
    <script src='./includes/plugins/timepicker/bootstrap-timepicker.min.js'></script>
    <?php
        switch($mod){
            case 'documentos':
            case 'mis_aspi':
                echo "<script src='./js/documentos.js'></script>";
            break;
            case 'newaspirante':
                echo "<script src='./js/newaspirantes.js'></script>";
            break;
            case 'detalles':
                echo "<script src='./js/detalles.min.js'></script>";
            break;
            case 'inscribir':
                echo "<script src='./js/inscripcion.js'></script>"
                . "<script src='./includes/plugins/saycheese/say-cheese.js'></script>";
                break;
        }
    ?>
    
  </body>
</html>