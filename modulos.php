<?php
    session_name("loginUsuario");
    session_start();
    include("asset/clases/dbconectar.php");
    include("asset/clases/ConexionMySQL.php");
    $conn = new HelperMySql($mySQLserver,$mySQLuser,$mySQLpasswd,$mySQLbd);
    if(!isset($_GET["n"])){
        (@__DIR__ == '__DIR__') && define('__DIR__',  realpath(dirname(__FILE__)));
        $html = "";
        $file = __DIR__.'/html/menus.html';
        $template = file_get_contents($file);
        
        $sql = "SELECT * FROM sucursales where N_Sucursal in({$_SESSION["ses_childs"]})";
        $id = $conn->query($sql);
        while ($row = $conn->fetch($id)){
            $html .= "<a href='?n={$row["N_Sucursal"]}' class='btn btn-block btn-social btn-facebook btn-flat'><i class='fa fa-institution'></i> {$row["Nombre"]}</a>";
        }
        $template = str_replace('{sucursales}', $html, $template);
        //print_r($_SESSION);
        print $template;
    }else{
        $_GET = array_map("htmlspecialchars", $_GET);
        $sql = "SELECT * FROM sucursales where N_Sucursal in({$_GET["n"]})";
        $row = $conn->fetch($conn->query($sql));
        $_SESSION['sucursal']= $row['N_Sucursal']; 
        $_SESSION['nombre_sucursal']= $row['Nombre'];
        $_SESSION["ses_acumulable"] = $row["acumulable"];
        if (strpos ($row["Nombre"],"Icep") === false ){
            if (strpos ($row["Nombre"],"Casserole") === false ){
                $_SESSION["tipo_escuela"]	= "pece";
            }else{
                $_SESSION["tipo_escuela"]	= "casserole";
            }
        }else{ 
            $_SESSION["tipo_escuela"]	= "icep";
        }
        header("Location: asset/");
    }

