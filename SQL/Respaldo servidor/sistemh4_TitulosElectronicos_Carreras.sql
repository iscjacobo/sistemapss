CREATE DATABASE  IF NOT EXISTS `sistemh4_TitulosElectronicos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sistemh4_TitulosElectronicos`;
-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: sistematece.com    Database: sistemh4_TitulosElectronicos
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Carreras`
--

DROP TABLE IF EXISTS `Carreras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carreras` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `_idCampus` int(11) NOT NULL,
  `_idCarrera` int(11) NOT NULL,
  `cveCarrera` int(11) NOT NULL,
  `Carrera` text NOT NULL,
  `_idTipoPeriodo` text NOT NULL,
  `ClavePlan` text NOT NULL,
  `_idNivelEstudios` text NOT NULL,
  `CalificacionMinima` text NOT NULL,
  `CalificacionMaxima` text NOT NULL,
  `CalificacionMinimaAprobatoria` text NOT NULL,
  `Rvoe` int(11) NOT NULL,
  `FechaExpedicionRVOE` date NOT NULL,
  `Modulos` int(11) NOT NULL,
  `USRCreacion` text NOT NULL,
  `FechaCreacion` date NOT NULL,
  `USRModifico` text NOT NULL,
  `FechaModifico` date NOT NULL,
  `Estatus` tinytext NOT NULL,
  `_idAutorizacionReconocimiento` int(11) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `fk_Carreras_1_idx` (`_idCampus`),
  CONSTRAINT `fk_Carreras_1` FOREIGN KEY (`_idCampus`) REFERENCES `Campus` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Carreras`
--

LOCK TABLES `Carreras` WRITE;
/*!40000 ALTER TABLE `Carreras` DISABLE KEYS */;
INSERT INTO `Carreras` VALUES (1,2,20161,6713040,'LICENCIATURA EN COMERCIO Y ADINISTRACION DE ADUANAS','6','2017','1','5','10','7',20171526,'2017-11-07',7,'arosast@hotmail.com','2019-11-19','arosast@hotmail.com','2019-11-20','1',0),(2,2,516344,0,'INGENIERIA EN GESTIÓN EMPRESARIAL','6','2017','1','5','10','7',20171527,'2017-11-07',8,'arosast@hotmail.com','2019-11-19','arosast@hotmail.com','2019-11-19','0',0),(3,2,20163,516344,'INGENIERIA EN GESTIÓN EMPRESARIAL','6','2017','1','5','10','7',20171527,'2017-11-07',8,'arosast@hotmail.com','2019-11-19','arosast@hotmail.com','2019-11-20','1',0);
/*!40000 ALTER TABLE `Carreras` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-21 10:06:31
