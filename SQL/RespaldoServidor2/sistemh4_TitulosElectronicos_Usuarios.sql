CREATE DATABASE  IF NOT EXISTS `sistemh4_TitulosElectronicos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sistemh4_TitulosElectronicos`;
-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: sistematece.com    Database: sistemh4_TitulosElectronicos
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Usuarios`
--

DROP TABLE IF EXISTS `Usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuarios` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` text COLLATE big5_bin NOT NULL,
  `Ap_Paterno` text COLLATE big5_bin NOT NULL,
  `Ap_Materno` text COLLATE big5_bin NOT NULL,
  `email` text COLLATE big5_bin NOT NULL,
  `Username` text COLLATE big5_bin NOT NULL,
  `USRCreacion` text COLLATE big5_bin NOT NULL,
  `FechaCreacion` date NOT NULL,
  `USRModifico` text COLLATE big5_bin NOT NULL,
  `FechaModifico` date NOT NULL,
  `Estatus` tinyint(1) NOT NULL,
  `_idCampus` int(11) NOT NULL,
  `_idInstitucion` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=big5 COLLATE=big5_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuarios`
--

LOCK TABLES `Usuarios` WRITE;
/*!40000 ALTER TABLE `Usuarios` DISABLE KEYS */;
INSERT INTO `Usuarios` VALUES (1,'Francisco Ivan','Ramirez','Alcaraz','gforce3002@gmail.com','root','SYS','2019-11-19','SYS','2019-11-19',1,0,0),(2,'Jose Manuel','Jacobo','Urzua','jacobourzuamanuel@gmail.com','jacobourzuamanuel@gmail.com','SYS','2019-11-19','SYS','2019-11-19',1,0,0),(3,'Antonio','Rosas','Tapia','arosast@hotmail.com','arosast@hotmail.com','root','2019-11-19','root','2019-11-19',1,0,1),(4,'COINTA','MORALES','CABRERA','coni_psiorg@hotmail.com','coni_psiorg@hotmail.com','arosast@hotmail.com','2019-11-20','arosast@hotmail.com','2019-11-20',1,2,1);
/*!40000 ALTER TABLE `Usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  8:44:06
