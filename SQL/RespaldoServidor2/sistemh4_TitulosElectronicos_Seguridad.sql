CREATE DATABASE  IF NOT EXISTS `sistemh4_TitulosElectronicos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sistemh4_TitulosElectronicos`;
-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: sistematece.com    Database: sistemh4_TitulosElectronicos
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Seguridad`
--

DROP TABLE IF EXISTS `Seguridad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Seguridad` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` text NOT NULL,
  `Password` text NOT NULL,
  `Tipo_usuario` text NOT NULL,
  `Estatus` tinyint(1) NOT NULL,
  `USRCreacion` text NOT NULL,
  `FechaCreacion` date NOT NULL,
  `USRModifico` text NOT NULL,
  `FechaModifico` date NOT NULL,
  `_idUsuarios` int(11) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `fk_Seguridad_1_idx` (`_idUsuarios`),
  CONSTRAINT `fk_Seguridad_1` FOREIGN KEY (`_idUsuarios`) REFERENCES `Usuarios` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Seguridad`
--

LOCK TABLES `Seguridad` WRITE;
/*!40000 ALTER TABLE `Seguridad` DISABLE KEYS */;
INSERT INTO `Seguridad` VALUES (1,'root','4f4f92245c170bd04249570993365d415a08577f','root',1,'SYS','2019-11-19','SYS','2019-11-19',1),(2,'jacobourzuamanuel@gmail.com','302658f386ca4b526e7b45486f1de587e8035230','root',1,'SYS','2019-11-19','SYS','2019-11-19',2),(3,'arosast@hotmail.com','7125a0b98e70697fd496f865173631caaccd072f','Admin',1,'root','2019-11-19','root','2019-11-19',3),(4,'coni_psiorg@hotmail.com','1a6bc57125946b0d0ea927fe83729a976f858065','CtrlEsc',1,'arosast@hotmail.com','2019-11-20','arosast@hotmail.com','2019-11-20',4);
/*!40000 ALTER TABLE `Seguridad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  8:43:58
