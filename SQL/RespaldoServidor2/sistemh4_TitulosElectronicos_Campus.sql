CREATE DATABASE  IF NOT EXISTS `sistemh4_TitulosElectronicos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sistemh4_TitulosElectronicos`;
-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: sistematece.com    Database: sistemh4_TitulosElectronicos
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Campus`
--

DROP TABLE IF EXISTS `Campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Campus` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` text NOT NULL,
  `Domicilio` text NOT NULL,
  `Colonia` text NOT NULL,
  `CP` text NOT NULL,
  `Ciudad` text NOT NULL,
  `_idEstado` text NOT NULL,
  `_idInstitucion` int(11) NOT NULL,
  `Registro_DGP` text NOT NULL COMMENT 'Este campo es para ingresar la clave de regitro del campus ante la direccion de registros profesionales',
  `USRCreacion` text NOT NULL,
  `FechaCreacion` date NOT NULL,
  `USRModifico` text,
  `FechaModifico` date DEFAULT NULL,
  `Estatus` double DEFAULT NULL,
  PRIMARY KEY (`_id`),
  KEY `fk_Campus_1_idx` (`_idInstitucion`),
  CONSTRAINT `fk_Campus_1` FOREIGN KEY (`_idInstitucion`) REFERENCES `Instituciones` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Campus`
--

LOCK TABLES `Campus` WRITE;
/*!40000 ALTER TABLE `Campus` DISABLE KEYS */;
INSERT INTO `Campus` VALUES (1,'URUAPAN','INDEPENDENCIA #19','CENTRO','60000','URUAPAN','16',1,'160312','prueba@gmail.com','2019-11-19','prueba@gmail.com','2019-11-19',1),(2,'LAZARO CARDENAS','MELCHOR OCAMPO #','ZONA EJIDAL','6045','LAZARO CARDENAS','16',1,'160313','arosast@hotmail.com','2019-11-19','arosast@hotmail.com','2019-11-19',1);
/*!40000 ALTER TABLE `Campus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  8:44:08
